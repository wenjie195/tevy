<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid  = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:image" content="https://tevy.asia/img/fb-meta.jpg" />
<meta property="og:title" content="Upload Article | Tevy" />
<meta property="og:description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="keywords" content="Tevy, girls, female, lady, ladies, news, beauty care, beauty, skin care, fashion, social, etc">

<link rel="canonical" href="https://tevy.asia/userUploadArticles.php" />
<title>Upload Article | Tevy</title>
<?php include 'css.php'; ?>

<!-- <script src="ckeditor/ckeditor.js"></script> -->
<script src="//cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>

</head>

<body>
<?php include 'header-after-login.php'; ?>

<div class="background-div">
    <div class="cover-gap content min-height2">
        <div class="big-white-div same-padding">
             

        <form action="utilities/addNewArticlesNewFunction.php" method="POST" enctype="multipart/form-data">

        	<h1 class="landing-h1 margin-left-0"><?php echo _UPLOAD_ARTICLE_NEW ?></h1>    

                <div class="input-div">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_TITLE ?></p>
                    <textarea class="one-line-textarea aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_TITLE ?>" id="title" name="title" required></textarea>
                </div>  

                <div class="input-div">
                    <p class="input-top-text darkpink-text"><?php echo _UPLOAD_ARTICLE_LINK ?> <img src="img/refer.png" class="refer-png opacity-hover open-referlink" alt="<?php echo _UPLOAD_ARTICLE_REFER_ARTICLE_LINK ?>" title="<?php echo _UPLOAD_ARTICLE_REFER_ARTICLE_LINK ?>"></p>
                    <textarea class="one-line-textarea aidex-input clean" type="text" placeholder="how-to-stay-healthy" id="article_link" name="article_link" required></textarea>
                </div>   

                <div class="input-div">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_GOOGLE_KEYWORD ?> <img src="img/refer3.png" class="refer-png opacity-hover open-referkeyword" alt="<?php echo _UPLOAD_ARTICLE_REFER_GOOGLE_KEYWORD ?>" title="<?php echo _UPLOAD_ARTICLE_REFER_GOOGLE_KEYWORD ?>"></p>
                    <textarea class="three-line-textarea aidex-input clean" type="text" placeholder="beautiful, skin care, health," id="keyword_two" name="keyword_two" required></textarea>
                </div>  

                <div class="input-div">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_AUTHOR ?></p>
                    <input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_AUTHOR ?>" id="author_name" name="author_name" required>
                </div>  

                <div class="form-group input-div">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_CATEGORY ?></p>
                    <select class="clean aidex-input" type="text" id="type" name="type" required>
                        <option value="" name=" "><?php echo _UPLOAD_ARTICLE_CHOOSE_A_CATEGORY ?></option>
                        <option value="Beauty" name="Beauty"><?php echo _HEADER_BEAUTY ?></option>
                        <option value="Fashion" name="Fashion"><?php echo _HEADER_FASHION ?></option>
                        <option value="Social" name="Social"><?php echo _HEADER_SOCIAL ?></option>

                        <option value="Romance" name="Romance"><?php echo _HEADER_ROMANCE ?></option>
                        <option value="Entrepreneurship" name="Entrepreneurship"><?php echo _HEADER_ENTREPRENEURSHIP ?></option>
                        <option value="Diy" name="Diy"><?php echo _HEADER_DIY ?></option>
                        <option value="Cooking" name="Cooking"><?php echo _HEADER_COOKING ?></option>
                        <option value="Esport" name="Esport"><?php echo _HEADER_ESPORT ?></option>
                        <option value="Lifestyle" name="Lifestyle"><?php echo _HEADER_LIFESTYLE ?></option>
                        <option value="Pets" name="Pets"><?php echo _HEADER_PETS ?></option>
                        <option value="Travel" name="Travel"><?php echo _HEADER_TRAVEL ?></option>
                    </select>    
                </div>

                <div class="input-div">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO ?></p>
                    <input id="file-upload" type="file" name="cover_photo" id="cover_photo" accept="image/*" required>    
                </div>   

                <div class="para-photo">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p>        
                    <input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>" id="cover_photo_source" name="cover_photo_source" > 
                </div>

                <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_DESCRIPTION ?>  <img src="img/refer.png" class="refer-png opacity-hover open-referde" alt="<?php echo _UPLOAD_ARTICLE_WHAT_IS_DESCRIPTION ?>" title="<?php echo _UPLOAD_ARTICLE_WHAT_IS_DESCRIPTION ?>"></p>
                    <!-- <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_DESCRIPTION ?> <img src="img/refer.png" class="refer-png opacity-hover open-referdesc" alt="<?php echo _UPLOAD_ARTICLE_REFER_DESCRIPTION ?>" title="<?php echo _UPLOAD_ARTICLE_REFER_DESCRIPTION ?>"></p> -->
                    <textarea class="aidex-input clean tevy-textarea" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_DESCRIPTION ?>" id="descprition" name="descprition" required></textarea>
                </div>  

                <div class="form-group publish-border input-div">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_BODY_TEXT ?>  <img src="img/refer.png" class="refer-png opacity-hover open-refertexteditor" alt="<?php echo _UPLOAD_ARTICLE_BODY_TEXT ?>" title="<?php echo _UPLOAD_ARTICLE_BODY_TEXT ?>"> <span class="darkpink-text"><?php echo _UPLOAD_ARTICLE_BODY_TEXT_FAQ ?></span></p>
                    <textarea name="editor" id="editor" rows="10" cols="80"></textarea>
                </div>

                <!-- <div class="para-photo">
                    <p class="input-top-text"><?php //echo _UPLOAD_ARTICLE_YOUTUBE_VIDEO ?> 1 <img src="img/refer.png" class="refer-png opacity-hover open-refer" alt="<?php //echo _UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE ?>" title="<?php //echo _UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE ?>"></p>
                    <input class="aidex-input clean" type="text" placeholder="<?php //echo _UPLOAD_ARTICLE_YOUTUBE_VIDEO_LINK ?>" id="youtube_link_one" name="youtube_link_one">
                </div>  

                <div class="para-photo">
                    <p class="input-top-text"><?php //echo _UPLOAD_ARTICLE_YOUTUBE_VIDEO ?> 2 <img src="img/refer3.png" class="refer-png opacity-hover open-refer" alt="<?php //echo _UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE ?>" title="<?php //echo _UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE ?>"></p>
                    <input class="aidex-input clean" type="text" placeholder="<?php //echo _UPLOAD_ARTICLE_YOUTUBE_VIDEO_LINK ?>" id="youtube_link_two" name="youtube_link_two">
                </div>  

                <div class="para-photo">
                    <p class="input-top-text"><?php //echo _UPLOAD_ARTICLE_YOUTUBE_VIDEO ?> 3 <img src="img/refer3.png" class="refer-png opacity-hover open-refer" alt="<?php //echo _UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE ?>" title="<?php //echo _UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE ?>"></p>
                    <input class="aidex-input clean" type="text" placeholder="<?php //echo _UPLOAD_ARTICLE_YOUTUBE_VIDEO_LINK ?>" id="youtube_link_three" name="youtube_link_three">
                </div> 

                <div class="para-photo">
                    <p class="input-top-text"><?php //echo _UPLOAD_ARTICLE_YOUTUBE_VIDEO ?> 4 <img src="img/refer3.png" class="refer-png opacity-hover open-refer" alt="<?php //echo _UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE ?>" title="<?php //echo _UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE ?>"></p>
                    <input class="aidex-input clean" type="text" placeholder="<?php //echo _UPLOAD_ARTICLE_YOUTUBE_VIDEO_LINK ?>" id="youtube_link_four" name="youtube_link_four">
                </div>  

                <div class="para-photo">
                    <p class="input-top-text"><?php //echo _UPLOAD_ARTICLE_YOUTUBE_VIDEO ?> 5 <img src="img/refer3.png" class="refer-png opacity-hover open-refer" alt="<?php //echo _UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE ?>" title="<?php //echo _UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE ?>"></p>
                    <input class="aidex-input clean" type="text" placeholder="<?php //echo _UPLOAD_ARTICLE_YOUTUBE_VIDEO_LINK ?>" id="youtube_link_five" name="youtube_link_five">
                </div>   -->

                <button class="clean-button clean login-btn pink-button" name="submit"><?php echo _UPLOAD_ARTICLE_SUBMIT ?></button>

        </form>

        </div>
    </div>
</div>

<script>
    CKEDITOR.replace('editor');
</script>

<?php include 'footer.php'; ?>

</body>
</html>                 