<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/ArticleOne.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userUid = $_SESSION['uid'];

// $timestamp = time();

function uploadNewArticles($conn,$uid,$authorUid,$authorName,$title,$seoTitle,$keywordTwo,$articleLink,$author,$titleCover,$imgCoverSrc,$keywordOne,$paragraphOne,$type)
{
     if(insertDynamicData($conn,"articles_one",array("uid","author_uid","author_name","title","seo_title","keyword_two","article_link","author","title_cover","img_cover_source",
                              "keyword_one","paragraph_one","type"),
          array($uid,$authorUid,$authorName,$title,$seoTitle,$keywordTwo,$articleLink,$author,$titleCover,$imgCoverSrc,$keywordOne,$paragraphOne,$type),"sssssssssssss") === null)
     {
          header('Location: ../userUploadArticles.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");

     $authorUid = $userDetails[0]->getUid();
     $authorName = $userDetails[0]->getUsername();

     $title = rewrite($_POST['title']);

     $articleLink = rewrite($_POST['article_link']);

     $str = str_replace( array(' ',','), '', $title);
     $strkeywordOne = str_replace( array(','), '', $articleLink);
     $AddToEnd = "-";
     $seoTitle = $str.$AddToEnd.$strkeywordOne;

     $keywordTwo = rewrite($_POST['keyword_two']);
     $author = rewrite($_POST['author_name']);

     $imgCoverSrc = rewrite($_POST['cover_photo_source']);

     $titleCover = $_FILES['cover_photo']['name'];
     // $titleCover = $timestamp.$_FILES['cover_photo']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["cover_photo"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['cover_photo']['tmp_name'],$target_dir.$titleCover);
     }

     // $paragraphOne = rewrite($_POST['paragraph_one']);

     $keywordOne = rewrite($_POST['descprition']);

     // $paragraphTwo = ($_POST['editor']);  //no rewrite, cause error in db

     $paragraphOne = ($_POST['editor']);  //no rewrite, cause error in db

     // $imageOne = $_FILES['image_one']['name'];
     // $target_dir = "../uploads/";
     // $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
     // // Select file type
     // $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // // Valid file extensions
     // $extensions_arr = array("jpg","jpeg","png","gif");
     // if( in_array($imageFileType,$extensions_arr) )
     // {
     //      move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imageOne);
     // }

     // $imgOneSrc = rewrite($_POST['image_one_source']);

     // $uTubeLinkOne = rewrite($_POST['youtube_link_one']);
     // if($uTubeLinkOne != '')
     // {
     //      $tubeOne = "Yes";
     // }
     // else
     // {
     //      $tubeOne = "No";
     // }

     // $uTubeLinkTwo = rewrite($_POST['youtube_link_two']);
     // if($uTubeLinkTwo != '')
     // {
     //      $tubeTwo = "Yes";
     // }
     // else
     // {
     //      $tubeTwo = "No";
     // }

     // $uTubeLinkThree = rewrite($_POST['youtube_link_three']);
     // if($uTubeLinkThree != '')
     // {
     //      $tubeThree = "Yes";
     // }
     // else
     // {
     //      $tubeThree = "No";
     // }

     // $uTubeLinkFour = rewrite($_POST['youtube_link_four']);
     // if($uTubeLinkFour != '')
     // {
     //      $tubeFour = "Yes";
     // }
     // else
     // {
     //      $tubeFour = "No";
     // }

     // $uTubeLinkFive = rewrite($_POST['youtube_link_five']);
     // if($uTubeLinkFive != '')
     // {
     //      $tubeFive = "Yes";
     // }
     // else
     // {
     //      $tubeFive = "No";
     // }

     $type = rewrite($_POST['type']);

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $title."<br>";
     // echo $paragraphOne."<br>";
     // echo $type."<br>";

     if(uploadNewArticles($conn,$uid,$authorUid,$authorName,$title,$seoTitle,$keywordTwo,$articleLink,$author,$titleCover,$imgCoverSrc,$keywordOne,$paragraphOne,$type))
     {
          // echo "articles upload successfully";
          // echo "<script>alert('Register Success !');window.location='../addReferee.php'</script>";    

          if($type == 'Beauty')
          {
               header('Location: ../beautyCare.php');
          }
          elseif($type == 'Fashion')
          {
               header('Location: ../trendyFashion.php');
          }
          elseif($type == 'Social')
          {
               header('Location: ../socialNews.php');
          }
          else
          {
               echo "error";
          }

     }
     else
     {
          echo "fail to upload article";
     }
  
}
else 
{
     header('Location: ../index.php');
}

?>