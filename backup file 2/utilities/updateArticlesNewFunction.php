<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/ArticleOne.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userUid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = $userUid;

     $articleUid = rewrite($_POST['article_uid']);
     $title = rewrite($_POST['update_title']);
     $articleLink = rewrite($_POST['update_article_link']);
     $keywordTwo = rewrite($_POST['update_keyword_two']);
     $author = rewrite($_POST['update_author']);
     $str = str_replace( array(' ',','), '', $title);
     $strkeywordOne = str_replace( array(','), '', $articleLink);
     $AddToEnd = "-";
     $seoTitle = $str.$AddToEnd.$strkeywordOne;

     $type = rewrite($_POST['update_type']);

     $coverPhoto = $_FILES['update_cover_photo']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["update_cover_photo"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['update_cover_photo']['tmp_name'],$target_dir.$coverPhoto);
     }

     // $imgCoverSrc = rewrite($_POST['update_cover_photo_source']);
     $imgCoverSrcData = rewrite($_POST['update_cover_photo_source']);
     if($imgCoverSrcData ==  '')
     {
          $imgCoverSrc = "No";
     }
     else
     {
          $imgCoverSrc = rewrite($_POST['update_cover_photo_source']);
     }

     $keywordOne = rewrite($_POST['update_description']);

     $paragraphOne = ($_POST['editor']);  //no rewrite, cause error in db

     // $uTubeLinkOne = rewrite($_POST['update_youtube_link_one']);
     // $uTubeLinkTwo = rewrite($_POST['update_youtube_link_two']);
     // $uTubeLinkThree = rewrite($_POST['update_youtube_link_three']);
     // $uTubeLinkFour = rewrite($_POST['update_youtube_link_four']);
     // $uTubeLinkFive = rewrite($_POST['update_youtube_link_five']);
          
     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $country."<br>";
     // echo $firstname."<br>";
     // echo $lastname."<br>";
     // echo $prooftype."<br>";

     if(isset($_POST['submit']))
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          // //echo "save to database";
          if($title)
          {
               array_push($tableName,"title");
               array_push($tableValue,$title);
               $stringType .=  "s";
          }
          if($articleLink)
          {
               array_push($tableName,"article_link");
               array_push($tableValue,$articleLink);
               $stringType .=  "s";
          }
          if($keywordTwo)
          {
               array_push($tableName,"keyword_two");
               array_push($tableValue,$keywordTwo);
               $stringType .=  "s";
          }
          if($author)
          {
               array_push($tableName,"author");
               array_push($tableValue,$author);
               $stringType .=  "s";
          }
          if($seoTitle)
          {
               array_push($tableName,"seo_title");
               array_push($tableValue,$seoTitle);
               $stringType .=  "s";
          }
          if($type)
          {
               array_push($tableName,"type");
               array_push($tableValue,$type);
               $stringType .=  "s";
          }
          if($coverPhoto)
          {
               array_push($tableName,"title_cover");
               array_push($tableValue,$coverPhoto);
               $stringType .=  "s";
          }
          if($imgCoverSrc)
          {
               array_push($tableName,"img_cover_source");
               array_push($tableValue,$imgCoverSrc);
               $stringType .=  "s";
          }
          if($keywordOne)
          {
               array_push($tableName,"keyword_one");
               array_push($tableValue,$keywordOne);
               $stringType .=  "s";
          }
          if($paragraphOne)
          {
               array_push($tableName,"paragraph_one");
               array_push($tableValue,$paragraphOne);
               $stringType .=  "s";
          }
          // if($uTubeLinkOne)
          // {
          //      array_push($tableName,"utube_linkone");
          //      array_push($tableValue,$uTubeLinkOne);
          //      $stringType .=  "s";
          // }
          // if($uTubeLinkTwo)
          // {
          //      array_push($tableName,"utube_linktwo");
          //      array_push($tableValue,$uTubeLinkTwo);
          //      $stringType .=  "s";
          // }
          // if($uTubeLinkThree)
          // {
          //      array_push($tableName,"utube_linkthree");
          //      array_push($tableValue,$uTubeLinkThree);
          //      $stringType .=  "s";
          // }
          // if($uTubeLinkFour)
          // {
          //      array_push($tableName,"utube_linkfour");
          //      array_push($tableValue,$uTubeLinkFour);
          //      $stringType .=  "s";
          // }
          // if($uTubeLinkFive)
          // {
          //      array_push($tableName,"utube_linkfive");
          //      array_push($tableValue,$uTubeLinkFive);
          //      $stringType .=  "s";
          // }
          
          array_push($tableValue,$articleUid);
          $stringType .=  "s";
          $updateArticles = updateDynamicData($conn,"articles_one"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($updateArticles)
          {

               if($type == 'Beauty')
               {
                    header('Location: ../beautyCare.php');
               }
               elseif($type == 'Fashion')
               {
                    header('Location: ../trendyFashion.php');
               }
               elseif($type == 'Social')
               {
                    header('Location: ../socialNews.php');
               }
               else
               {
                    echo "error";
               }

          }
          else
          {    
               header('Location: ../viewArticles.php?type=2');
          }
     }
     else
     {
          header('Location: ../viewArticles.php?type=3');
     }
  
}
else 
{
     header('Location: ../index.php');
}
?>