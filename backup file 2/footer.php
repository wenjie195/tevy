<div class="footer-red">
<p class="footer-p">© 2020 <?php echo _FOOTER_ALL_RIGHT ?></p>
</div>
<!-- Login Modal -->
<div id="login-modal" class="modal-css">
	<div class="modal-content-css login-modal-content">
        <span class="close-css close-login">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 darkpink-text login-h1"><?php echo _JS_LOGIN ?></h1>	   
   <div class="big-white-div">
		<div class="login-div">
         <form action="utilities/loginFunction.php" method="POST">
            <div class="login-input-div">
                <p class="input-top-text"><?php echo _JS_USERNAME ?></p>
                <input class="aidex-input clean" type="text" placeholder="<?php echo _JS_USERNAME ?>" id="username" name="username" required>
            </div>  
            <div class="login-input-div">
                <p class="input-top-text"><?php echo _JS_PASSWORD ?></p>
                <input class="aidex-input clean" type="password" placeholder="<?php echo _JS_PASSWORD ?>" id="password" name="password" required>
            </div>   

            <button class="clean-button clean login-btn pink-button" name="login"><?php echo _JS_LOGIN ?></button>
        </form>
          
      </div>                 
 </div>
        
                
	</div>

</div>
<!-- Refer Modal -->
<div id="refer-modal" class="modal-css">
	<div class="modal-content-css refer-modal-content">
        <span class="close-css close-refer">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 darkpink-text login-h1"><?php echo _UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE ?></h1>	   
	<div class="clear"></div>
    <div class="width100 overflow tutorial-div">
    	<p class="step-p"><?php echo _UPLOAD_ARTICLE_YOUTUBE_ID_STEP1 ?></p>
        <img src="img/desktop-view-step1.jpg" class="width100" alt="<?php echo _UPLOAD_ARTICLE_YOUTUBE_ID_STEP1 ?>" title="<?php echo _UPLOAD_ARTICLE_YOUTUBE_ID_STEP1 ?>">
        <p class="step-p"><?php echo _UPLOAD_ARTICLE_YOUTUBE_ID_STEP2 ?></p>
        <img src="img/desktop-view-step2.jpg" class="width100" alt="<?php echo _UPLOAD_ARTICLE_YOUTUBE_ID_STEP2 ?>" title="<?php echo _UPLOAD_ARTICLE_YOUTUBE_ID_STEP2 ?>">
        <p class="step-p"><?php echo _UPLOAD_ARTICLE_YOUTUBE_ID_ALTERNATE_STEP ?></p>
        <img src="img/desktop-view-step3.png" class="width100" alt="<?php echo _UPLOAD_ARTICLE_YOUTUBE_ID_ALTERNATE_STEP ?>" title="<?php echo _UPLOAD_ARTICLE_YOUTUBE_ID_ALTERNATE_STEP ?>">        
    	<p class="step-p"><?php echo _UPLOAD_ARTICLE_YOUTUBE_APP_STEP1 ?></p>
        <img src="img/youtube-app-step1.jpg" class="width100" alt="<?php echo _UPLOAD_ARTICLE_YOUTUBE_APP_STEP1 ?>" title="<?php echo _UPLOAD_ARTICLE_YOUTUBE_APP_STEP1 ?>">     
    	<p class="step-p"><?php echo _UPLOAD_ARTICLE_YOUTUBE_APP_STEP2 ?></p>
        <img src="img/youtube-app-step2.jpg" class="width100" alt="<?php echo _UPLOAD_ARTICLE_YOUTUBE_APP_STEP2 ?>" title="<?php echo _UPLOAD_ARTICLE_YOUTUBE_APP_STEP2 ?>">            
    	<p class="step-p"><?php echo _UPLOAD_ARTICLE_YOUTUBE_APP_STEP3 ?></p>
        <img src="img/youtube-app-step3.jpg" class="width100" alt="<?php echo _UPLOAD_ARTICLE_YOUTUBE_APP_STEP3 ?>" title="<?php echo _UPLOAD_ARTICLE_YOUTUBE_APP_STEP3 ?>">            
    	<p class="step-p"><?php echo _UPLOAD_ARTICLE_YOUTUBE_APP_STEP4 ?></p>
        <img src="img/youtube-app-step4.png" class="width100" alt="<?php echo _UPLOAD_ARTICLE_YOUTUBE_APP_STEP4 ?>" title="<?php echo _UPLOAD_ARTICLE_YOUTUBE_APP_STEP4 ?>"> 
    </div>        
     <div class="width100 overflow">
     	<div class="clean-button clean close-btn pink-button close-refer"><?php echo _UPLOAD_ARTICLE_CLOSE ?></div>
     </div>           
	</div>

</div>
<!-- Refer Link Modal -->
<div id="referlink-modal" class="modal-css">
	<div class="modal-content-css refer-modal-content">
        <span class="close-css close-referlink">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 darkpink-text login-h1"><?php echo _UPLOAD_ARTICLE_WHAT_IS_ARTICLE_LINK ?></h1>	   
	<div class="clear"></div>
    <div class="width100 overflow tutorial-div">
    	<p class="step-p step-p2"><?php echo _UPLOAD_ARTICLE_WHAT_IS_ARTICLE_LINK_EXP ?></p>
        <img src="img/article-link.png" class="width100" alt="<?php echo _UPLOAD_ARTICLE_WHAT_IS_ARTICLE_LINK ?>" title="<?php echo _UPLOAD_ARTICLE_WHAT_IS_ARTICLE_LINK ?>">
    </div>        
     <div class="width100 overflow">
     	<div class="clean-button clean close-btn pink-button close-referlink"><?php echo _UPLOAD_ARTICLE_CLOSE ?></div>
     </div>           
	</div>
</div>
<!-- Refer Texteditor Modal -->
<div id="refertexteditor-modal" class="modal-css">
	<div class="modal-content-css refer-modal-content">
        <span class="close-css close-refertexteditor">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 darkpink-text login-h1"><?php echo _UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE ?></h1>	   
	<div class="clear"></div>
    <div class="width100 overflow tutorial-div">
    	<p class="step-p step-p2"><?php echo _UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE1 ?></p>
        <img src="img/tutorial-01.jpg" class="width100" alt="<?php echo _UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE ?>" title="<?php echo _UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE ?>">
    	<p class="step-p step-p2"><?php echo _UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE2 ?></p>
        <img src="img/desktop-view-step1.jpg" class="width100" alt="<?php echo _UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE ?>" title="<?php echo _UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE ?>">        
    	<p class="step-p step-p2"><?php echo _UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE3 ?></p>
        <img src="img/desktop-view-step2a.jpg" class="width100" alt="<?php echo _UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE ?>" title="<?php echo _UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE ?>">     
    	<p class="step-p step-p2"><?php echo _UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE4 ?></p>
        <img src="img/desktop-view-step3b.jpg" class="width100" alt="<?php echo _UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE ?>" title="<?php echo _UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE ?>">  
    	<p class="step-p step-p2"><?php echo _UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE5 ?></p>
        <img src="img/tutorial-02.jpg" class="width100" alt="<?php echo _UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE ?>" title="<?php echo _UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE ?>">         
    	<p class="step-p step-p2"><?php echo _UPLOAD_ARTICLE_HOW_TO_LINK ?></p>
        <img src="img/tutorial-03.jpg" class="width100" alt="<?php echo _UPLOAD_ARTICLE_HOW_TO_LINK ?>" title="<?php echo _UPLOAD_ARTICLE_HOW_TO_LINK ?>">    	<p class="step-p step-p2"><?php echo _UPLOAD_ARTICLE_HOW_TO_PLACE_IMAGE ?></p>
        <img src="img/tutorial-04.jpg" class="width100" alt="<?php echo _UPLOAD_ARTICLE_HOW_TO_PLACE_IMAGE ?>" title="<?php echo _UPLOAD_ARTICLE_HOW_TO_PLACE_IMAGE ?>">  
    	<p class="step-p step-p2"><?php echo _UPLOAD_ARTICLE_HOW_TO_PLACE_IMAGE2 ?></p>
        <img src="img/tutorial-05.jpg" class="width100" alt="<?php echo _UPLOAD_ARTICLE_HOW_TO_PLACE_IMAGE ?>" title="<?php echo _UPLOAD_ARTICLE_HOW_TO_PLACE_IMAGE ?>">         
    	<p class="step-p step-p2"><?php echo _UPLOAD_ARTICLE_HOW_TO_PLACE_IMAGE3 ?></p>
        <img src="img/tutorial-06.jpg" class="width100" alt="<?php echo _UPLOAD_ARTICLE_HOW_TO_PLACE_IMAGE ?>" title="<?php echo _UPLOAD_ARTICLE_HOW_TO_PLACE_IMAGE ?>">                            
    </div>        
     <div class="width100 overflow">
     	<div class="clean-button clean close-btn pink-button close-refertexteditor"><?php echo _UPLOAD_ARTICLE_CLOSE ?></div>
     </div>           
	</div>
</div>
<!-- Refer Keyword Modal -->
<div id="referde-modal" class="modal-css">
	<div class="modal-content-css refer-modal-content">
        <span class="close-css close-referde">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 darkpink-text login-h1"><?php echo _UPLOAD_ARTICLE_WHAT_IS_DESCRIPTION ?></h1>	   
	<div class="clear"></div>
    <div class="width100 overflow tutorial-div">
    	<p class="step-p step-p2"><?php echo _UPLOAD_ARTICLE_WHAT_IS_DESCRIPTION_EXP ?></p>
        <img src="img/description.jpg" class="width100" alt="<?php echo _UPLOAD_ARTICLE_WHAT_IS_DESCRIPTION ?>" title="<?php echo _UPLOAD_ARTICLE_WHAT_IS_DESCRIPTION ?>">
    </div>        
     <div class="width100 overflow">
     	<div class="clean-button clean close-btn pink-button close-referde"><?php echo _UPLOAD_ARTICLE_CLOSE ?></div>
     </div>           
	</div>
</div>

<!-- Refer Keyword Modal -->
<div id="referkeyword-modal" class="modal-css">
	<div class="modal-content-css refer-modal-content">
        <span class="close-css close-referkeyword">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 darkpink-text login-h1"><?php echo _UPLOAD_ARTICLE_WHAT_IS_KEYWORD ?></h1>	   
	<div class="clear"></div>
    <div class="width100 overflow tutorial-div">
    	<p class="step-p step-p2"><?php echo _UPLOAD_ARTICLE_WHAT_IS_KEYWORD_EXP ?></p>
        <img src="img/meta-keyword.jpg" class="width100" alt="<?php echo _UPLOAD_ARTICLE_WHAT_IS_KEYWORD ?>" title="<?php echo _UPLOAD_ARTICLE_WHAT_IS_KEYWORD ?>">
    </div>        
     <div class="width100 overflow">
     	<div class="clean-button clean close-btn pink-button close-referkeyword"><?php echo _UPLOAD_ARTICLE_CLOSE ?></div>
     </div>           
	</div>
</div>

<script src="js/jquery-2.2.1.min.js" type="text/javascript"></script> 

<script>
document.onkeydown = function(e) {
    if(e.keyCode == 123) {
      return false;
     }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
     return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
     return false;
    }
    if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
     return false;
    }
    if(e.ctrlKey && e.keyCode == 'S'.charCodeAt(0)){
     return false;
    }
	 if(e.ctrlKey && e.keyCode == 'C'.charCodeAt(0)){
     return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)){
     return false;
    }      
 }
</script>



  
<script>
function printFunction() {
    window.print();
}
</script>

<script src="js/bootstrap.js"></script>
<script src="js/ie-emulation-modes-warning.js"></script>
<script src="js/ie10-viewport-bug-workaround.js"></script>





<script src="js/main-login.js"></script>
<script src="js/headroom.js"></script>
<script>
(function() {
    var header = new Headroom(document.querySelector("#header"), {
        tolerance: 5,
        offset : 205,
        classes: {
          initial: "animated",
          pinned: "slideDown",
          unpinned: "slideUp"
        }
    });
    header.init();

    var bttHeadroom = new Headroom(document.getElementById("btt"), {
        tolerance : 0,
        offset : 500,
        classes : {
            initial : "slide",
            pinned : "slide--reset",
            unpinned : "slide--down"
        }
    });
    bttHeadroom.init();
}());
</script>

<script src="js/jquery-2.2.1.min.js" type="text/javascript"></script> 

<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>
<script>

var loginmodal = document.getElementById("login-modal");
var deletemodal = document.getElementById("delete-modal");
var refermodal = document.getElementById("refer-modal");
var referlinkmodal = document.getElementById("referlink-modal");
var refertexteditormodal = document.getElementById("refertexteditor-modal");
var referdemodal = document.getElementById("referde-modal");
var referkeywordmodal = document.getElementById("referkeyword-modal");


var openlogin = document.getElementsByClassName("open-login")[0];
var openlogin1 = document.getElementsByClassName("open-login")[1];
var opendelete = document.getElementsByClassName("open-delete")[0];
var openrefer = document.getElementsByClassName("open-refer")[0];
var openrefer1 = document.getElementsByClassName("open-refer")[1];
var openrefer2 = document.getElementsByClassName("open-refer")[2];
var openrefer3 = document.getElementsByClassName("open-refer")[3];
var openrefer4 = document.getElementsByClassName("open-refer")[4];
var openreferlink = document.getElementsByClassName("open-referlink")[0];
var openrefertexteditor = document.getElementsByClassName("open-refertexteditor")[0];
var openreferde = document.getElementsByClassName("open-referde")[0];
var openreferkeyword = document.getElementsByClassName("open-referkeyword")[0];



var closelogin = document.getElementsByClassName("close-login")[0];
var closedelete = document.getElementsByClassName("close-delete")[0];
var closedelete1 = document.getElementsByClassName("close-delete")[1];
var closerefer = document.getElementsByClassName("close-refer")[0];
var closereferlink = document.getElementsByClassName("close-referlink")[0];
var closerefer1 = document.getElementsByClassName("close-refer")[1];
var closereferlink1 = document.getElementsByClassName("close-referlink")[1];
var closerefertexteditor = document.getElementsByClassName("close-refertexteditor")[0];
var closerefertexteditor1 = document.getElementsByClassName("close-refertexteditor")[1];
var closereferde = document.getElementsByClassName("close-referde")[0];
var closereferde1 = document.getElementsByClassName("close-referde")[1];
var closereferkeyword = document.getElementsByClassName("close-referkeyword")[0];
var closereferkeyword1 = document.getElementsByClassName("close-referkeyword")[1];

if(openlogin){
openlogin.onclick = function() {
  loginmodal.style.display = "block";
}
}
if(openlogin1){
openlogin1.onclick = function() {
  loginmodal.style.display = "block";
}
}
if(opendelete){
opendelete.onclick = function() {
  deletemodal.style.display = "block";
}
}
if(openrefer){
openrefer.onclick = function() {
  refermodal.style.display = "block";
}
}
if(openrefer1){
openrefer1.onclick = function() {
  refermodal.style.display = "block";
}
}
if(openrefer2){
openrefer2.onclick = function() {
  refermodal.style.display = "block";
}
}
if(openrefer3){
openrefer3.onclick = function() {
  refermodal.style.display = "block";
}
}
if(openrefer4){
openrefer4.onclick = function() {
  refermodal.style.display = "block";
}
}
if(openreferlink){
openreferlink.onclick = function() {
  referlinkmodal.style.display = "block";
}
}
if(openrefertexteditor){
openrefertexteditor.onclick = function() {
  refertexteditormodal.style.display = "block";
}
}
if(openreferde){
openreferde.onclick = function() {
  referdemodal.style.display = "block";
}
}
if(openreferkeyword){
openreferkeyword.onclick = function() {
  referkeywordmodal.style.display = "block";
}
}


if(closelogin){
  closelogin.onclick = function() {
  loginmodal.style.display = "none";
}
}
if(closedelete){
  closedelete.onclick = function() {
  deletemodal.style.display = "none";
}
}
if(closedelete1){
  closedelete1.onclick = function() {
  deletemodal.style.display = "none";
}
}
if(closerefer){
  closerefer.onclick = function() {
  refermodal.style.display = "none";
}
}
if(closerefer1){
  closerefer1.onclick = function() {
  refermodal.style.display = "none";
}
}
if(closereferlink){
  closereferlink.onclick = function() {
  referlinkmodal.style.display = "none";
}
}
if(closereferlink1){
  closereferlink1.onclick = function() {
  referlinkmodal.style.display = "none";
}
}
if(closereferkeyword){
  closereferkeyword.onclick = function() {
  referkeywordmodal.style.display = "none";
}
}
if(closereferkeyword1){
  closereferkeyword1.onclick = function() {
  referkeywordmodal.style.display = "none";
}
}
if(closerefertexteditor){
  closerefertexteditor.onclick = function() {
  refertexteditormodal.style.display = "none";
}
}
if(closerefertexteditor1){
  closerefertexteditor1.onclick = function() {
  refertexteditormodal.style.display = "none";
}
}
if(closereferde){
  closereferde.onclick = function() {
  referdemodal.style.display = "none";
}
}
if(closereferde1){
  closereferde1.onclick = function() {
  referdemodal.style.display = "none";
}
}


window.onclick = function(event) {

  if (event.target == loginmodal) {
    loginmodal.style.display = "none";
  }  
  if (event.target == deletemodal) {
    deletemodal.style.display = "none";
  }    
  if (event.target == refermodal) {
    refermodal.style.display = "none";
  }   
  if (event.target == referlinkmodal) {
    referlinkmodal.style.display = "none";
  } 
  if (event.target == refertexteditormodal) {
    refertexteditormodal.style.display = "none";
  }
  if (event.target == referdemodal) {
    referdemodal.style.display = "none";
  }        
  if (event.target == referkeywordmodal) {
    referkeywordmodal.style.display = "none";
  }  
    
}
</script>
<script>
function myFunction() {
  var x = document.getElementById("moreParagraph");
  var y = document.getElementById("addButton");
  if (x.style.display === "none") {
	x.style.display = "none";
  } else {
	x.style.display = "block";
	y.style.display = "none";
  }
}
</script>
