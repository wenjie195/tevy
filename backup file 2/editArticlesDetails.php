<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ArticleOne.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid  = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:image" content="https://tevy.asia/img/fb-meta.jpg" />
<meta property="og:title" content="Edit Article | Tevy" />
<meta property="og:description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="keywords" content="Tevy, girls, female, lady, ladies, news, beauty care, beauty, skin care, fashion, social, etc">
<link rel="canonical" href="https://tevy.asia/editNewsDetails.php" />

<title>Edit Article | Tevy</title>
<?php include 'css.php'; ?>

<!-- <script src="ckeditor/ckeditor.js"></script> -->
<script src="//cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>

</head>

<body>
<?php include 'header-after-login.php'; ?>

<div class="background-div">
    <div class="cover-gap content min-height2">
        <div class="big-white-div same-padding">

        <?php
        if(isset($_POST['news_uid']))
        {
        $conn = connDB();
        $articlesDetails = getArticlesOne($conn,"WHERE uid = ? ", array("uid") ,array($_POST['news_uid']),"s");
        ?>

       <!-- <form action="utilities/updateArticlesFunction.php" method="POST" enctype="multipart/form-data"> -->
       <form action="utilities/updateArticlesNewFunction.php" method="POST" enctype="multipart/form-data">
        	<h1 class="landing-h1 margin-left-0"><?php echo _EDIT_ARTICLE ?></h1>    

            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_TITLE ?></p>            
                <textarea class="one-line-textarea aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_TITLE ?>" id="update_title" name="update_title"><?php echo $articlesDetails[0]->getTitle(); ?></textarea>
            </div>  

            <div class="input-div">
                <p class="input-top-text darkpink-text"><?php echo _UPLOAD_ARTICLE_LINK ?> <img src="img/refer.png" class="refer-png opacity-hover open-referlink" alt="<?php echo _UPLOAD_ARTICLE_REFER_ARTICLE_LINK ?>" title="<?php echo _UPLOAD_ARTICLE_REFER_ARTICLE_LINK ?>"></p>
                <textarea class="three-line-textarea aidex-input clean" type="text" placeholder="how-to-stay-healthy" id="update_article_link" name="update_article_link"><?php echo $articlesDetails[0]->getArticleLink(); ?></textarea>
            </div>  

            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_GOOGLE_KEYWORD ?> <img src="img/refer3.png" class="refer-png opacity-hover open-referkeyword" alt="<?php echo _UPLOAD_ARTICLE_REFER_GOOGLE_KEYWORD ?>" title="<?php echo _UPLOAD_ARTICLE_REFER_GOOGLE_KEYWORD ?>"></p>
                <textarea class="three-line-textarea aidex-input clean" type="text" placeholder="beautiful, skin care, health" id="update_keyword_two" name="update_keyword_two"><?php echo $articlesDetails[0]->getKeywordTwo(); ?></textarea>
            </div>  

            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_AUTHOR ?></p>
                <input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_AUTHOR ?>" value="<?php echo $articlesDetails[0]->getAuthor(); ?>" id="update_author" name="update_author">
            </div>  

            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_CATEGORY ?> : <?php echo $articlesDetails[0]->getType(); ?></p>

                <select class="clean aidex-input" type="text" id="update_type" name="update_type" required>

                    <?php
                        if($articlesDetails[0]->getType() == 'Beauty')
                        {
                        ?>
                            <option selected value="Beauty"  name='Beauty'><?php echo _HEADER_BEAUTY ?></option>
                            <option value="Fashion"  name='Fashion'><?php echo _HEADER_FASHION ?></option>
                            <option value="Social"  name='Social'><?php echo _HEADER_SOCIAL ?></option>
                        <?php
                        }
                        else if($articlesDetails[0]->getType() == 'Fashion')
                        {
                        ?>
                            <option selected value="Fashion"  name='Fashion'><?php echo _HEADER_FASHION ?></option>
                            <option value="Beauty"  name='Beauty'><?php echo _HEADER_BEAUTY ?></option>
                            <option value="Social"  name='Social'><?php echo _HEADER_SOCIAL ?></option>
                        <?php
                        }
                        else if($articlesDetails[0]->getType() == 'Social')
                        {
                        ?>
                            <option selected value="Social"  name='Social'><?php echo _HEADER_SOCIAL ?></option>
                            <option value="Beauty"  name='Beauty'><?php echo _HEADER_BEAUTY ?></option>
                            <option value="Fashion"  name='Fashion'><?php echo _HEADER_FASHION ?></option>
                        <?php
                        }
                    ?>

                </select>                
            </div> 

            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO ?></p>
                <input id="file-upload" type="file" name="update_cover_photo" id="update_cover_photo" accept="image/*">      
                <img src="uploads/<?php echo $articlesDetails[0]->getTitleCover();;?>" class="width100 article-photo">
            </div>             

            <div class="para-photo">
            	<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p>        
             	<input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>" value="<?php echo $articlesDetails[0]->getImgCoverSrc(); ?>" id="update_cover_photo_source" name="update_cover_photo_source" > 
			</div>
            
            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_DESCRIPTION ?>  <img src="img/refer.png" class="refer-png opacity-hover open-referde" alt="<?php echo _UPLOAD_ARTICLE_WHAT_IS_DESCRIPTION ?>" title="<?php echo _UPLOAD_ARTICLE_WHAT_IS_DESCRIPTION ?>"></p>
                <textarea class="aidex-input clean tevy-textarea" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_DESCRIPTION ?>"  id="update_description" name="update_description"><?php echo $articlesDetails[0]->getKeywordOne(); ?></textarea>
            </div>              
            
            <div class="input-div form-group publish-border">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_BODY_TEXT ?>  <img src="img/refer.png" class="refer-png opacity-hover open-refertexteditor" alt="<?php echo _UPLOAD_ARTICLE_BODY_TEXT ?>" title="<?php echo _UPLOAD_ARTICLE_BODY_TEXT ?>"></p>
                <textarea name="editor" id="editor" rows="10" cols="80">
                    <?php echo $articlesDetails[0]->getParagraphOne(); ?>
                </textarea>
            </div>

            <!-- <div class="para-photo">
                <p class="input-top-text"><?php //echo _UPLOAD_ARTICLE_YOUTUBE_VIDEO ?> 1 <img src="img/refer.png" class="refer-png opacity-hover open-refer" alt="<?php //echo _UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE ?>" title="<?php //echo _UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE ?>"></p>
                <input class="aidex-input clean" type="text" placeholder="<?php //echo _UPLOAD_ARTICLE_YOUTUBE_VIDEO_LINK ?>" value="<?php //echo $articlesDetails[0]->getUtubeLinkOne(); ?>" id="update_youtube_link_one" name="update_youtube_link_one">
            </div>  

            <div class="para-photo">
                <p class="input-top-text"><?php //echo _UPLOAD_ARTICLE_YOUTUBE_VIDEO ?> 2 <img src="img/refer3.png" class="refer-png opacity-hover open-refer" alt="<?php //echo _UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE ?>" title="<?php //echo _UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE ?>"></p>
                <input class="aidex-input clean" type="text" placeholder="<?php //echo _UPLOAD_ARTICLE_YOUTUBE_VIDEO_LINK ?>" value="<?php //echo $articlesDetails[0]->getUtubeLinkTwo(); ?>" id="update_youtube_link_two" name="update_youtube_link_two">
            </div>  

            <div class="para-photo">
                <p class="input-top-text"><?php //echo _UPLOAD_ARTICLE_YOUTUBE_VIDEO ?> 3 <img src="img/refer3.png" class="refer-png opacity-hover open-refer" alt="<?php //echo _UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE ?>" title="<?php //echo _UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE ?>"></p>
                <input class="aidex-input clean" type="text" placeholder="<?php //echo _UPLOAD_ARTICLE_YOUTUBE_VIDEO_LINK ?>" value="<?php //echo $articlesDetails[0]->getUtubeLinkThree(); ?>" id="update_youtube_link_three" name="update_youtube_link_three">
            </div> 

            <div class="para-photo">
                <p class="input-top-text"><?php //echo _UPLOAD_ARTICLE_YOUTUBE_VIDEO ?> 4 <img src="img/refer3.png" class="refer-png opacity-hover open-refer" alt="<?php //echo _UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE ?>" title="<?php //echo _UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE ?>"></p>
                <input class="aidex-input clean" type="text" placeholder="<?php //echo _UPLOAD_ARTICLE_YOUTUBE_VIDEO_LINK ?>" value="<?php //echo $articlesDetails[0]->getUtubeLinkFour(); ?>" id="update_youtube_link_four" name="update_youtube_link_four">
            </div>  

            <div class="para-photo">
                <p class="input-top-text"><?php //echo _UPLOAD_ARTICLE_YOUTUBE_VIDEO ?> 5 <img src="img/refer3.png" class="refer-png opacity-hover open-refer" alt="<?php //echo _UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE ?>" title="<?php //echo _UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE ?>"></p>
                <input class="aidex-input clean" type="text" placeholder="<?php //echo _UPLOAD_ARTICLE_YOUTUBE_VIDEO_LINK ?>" value="<?php //echo $articlesDetails[0]->getUtubeLinkFive(); ?>" id="update_youtube_link_five" name="update_youtube_link_five">
            </div>   -->
            
            <input type="hidden" value="<?php echo $articlesDetails[0]->getUid(); ?>" id="article_uid" name="article_uid">

			<button class="clean-button clean login-btn pink-button" name="submit"><?php echo _EDIT_ARTICLE_SUBMIT ?></button>
    
        </form>

        <?php
        }
        ?>

    </div>
</div>

<script>
    CKEDITOR.replace('editor');
</script>

<?php include 'footer.php'; ?>

</body>
</html>                 