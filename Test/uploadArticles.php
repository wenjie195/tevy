<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid  = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:image" content="https://tevy.asia/img/fb-meta.jpg" />
<meta property="og:title" content="Upload Article | Tevy" />
<meta property="og:description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="keywords" content="Tevy, girls, female, lady, ladies, news, beauty care, beauty, skin care, fashion, social, etc">

<link rel="canonical" href="https://tevy.asia/userUploadArticles.php" />
<title>Upload Article | Tevy</title>
<?php include 'css.php'; ?>
<script src="ckeditor/ckeditor.js"></script>
</head>

<body>
<?php include 'header-after-login.php'; ?>

<div class="background-div">
    <div class="cover-gap content min-height2">
        <div class="big-white-div same-padding">
             

        <form action="utilities/addNewArticlesNewFunction.php" method="POST" enctype="multipart/form-data">

        	<h1 class="landing-h1 margin-left-0"><?php echo _UPLOAD_ARTICLE_NEW ?></h1>    

                <div class="form-group">
                    <h4  class="title-color"><b class="family"><?php echo _UPLOAD_ARTICLE_TITLE ?></b><h4>
                    <input type="text" class="form-control form-control-border border-fix" placeholder="<?php echo _UPLOAD_ARTICLE_TITLE ?>" id="title" name="title" required/>
                </div>

                <!-- <div class="form-group publish-border">
                    <h4 class="title-color"><b>Upload Feature Photo</b></h4>
                    <input id="upload" type="file"></input>
                    <a href="" id="upload_photo_article" class="btn btn-default upload-article-image"><span style="color:black;">Browse</span></a>
                </div> -->

                <div class="input-div">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO ?></p>
                    <input id="file-upload" type="file" name="cover_photo" id="cover_photo" accept="image/*" required>    
                </div>   

                <div class="form-group publish-border">
                    <h4 class="title-color"><b class="family">Body Text</b></h4>
                    <!-- <textarea id="post_article" name="area" class="form-control mceEditor" rows="2"></textarea> -->
                    <textarea name="editor" id="editor" rows="10" cols="80">
                    </textarea>
                </div>

                <div class="form-group">
                    <h4 class="title-color"><b class="family">Category</b></h4>
                    <select class="clean aidex-input" type="text" id="type" name="type" required>
                        <option value="" name=" "><?php echo _UPLOAD_ARTICLE_CHOOSE_A_CATEGORY ?></option>
                        <option value="Beauty" name="Beauty"><?php echo _HEADER_BEAUTY ?></option>
                        <option value="Fashion" name="Fashion"><?php echo _HEADER_FASHION ?></option>
                        <option value="Social" name="Social"><?php echo _HEADER_SOCIAL ?></option>
                    </select>    
                </div>

                <!-- <div class="mobile publish-border">
                    <div class="pull-right">
                        <a href="#" onclick="content()" class="pencil-a" title="html_embed"><i class="flaticon-edit-1 pencil-write" aria-hidden="true"></i></a>
                        <button type="button" class="btn btn-default draft"><b class="savedraft">Save as draft</b></button>
                        <button type="submit" class="btn btn-default save">Publish</button>
                    </div>
                </div> -->

                <div class="mobile publish-border">
                    <div class="pull-right">
                        <button class="clean-button clean login-btn pink-button" name="submit"><?php echo _UPLOAD_ARTICLE_SUBMIT ?></button>
                    </div>
                </div>

        </form>

    </div>
</div>

<script>
    CKEDITOR.replace('editor');
</script>

<?php include 'footer.php'; ?>

</body>
</html>                 