<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Editor.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
?>

<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Text Editor</title>
    <meta charset="UTF-8">
    <meta name="description">
    <?php include 'css.php'; ?>

    <script src="ckeditor/ckeditor.js"></script>
</head>

<body>
<?php
    $conn = connDB();
?>

<div class="container">
	<div >
    	<h1 class="tele-h1">Text Editor</h1>

        <form action="utilities/submit.php" method="POST">
            <textarea name="editor" id="editor" rows="10" cols="80">
            </textarea>
            <button name="submit">Submit</button>
         </form>
    </div>
</div>

<script>
    CKEDITOR.replace('editor');
</script>

</body>
</html>