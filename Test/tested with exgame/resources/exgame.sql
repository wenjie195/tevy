-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 16, 2020 at 10:11 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `exgame`
--

-- --------------------------------------------------------

--
-- Table structure for table `createaccount`
--

CREATE TABLE `createaccount` (
  `id` bigint(255) NOT NULL,
  `username` varchar(100) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `retype` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_type` varchar(50) NOT NULL,
  `picture_id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `createaccount`
--

INSERT INTO `createaccount` (`id`, `username`, `fullname`, `password`, `retype`, `email`, `user_type`, `picture_id`) VALUES
(1, 'admin', 'admin', '123', '123', 'admin@gmail.com', '0', NULL),
(2, 'user', 'user', '123', '123', 'user@gmail.com', '1', '1581654035.png'),
(3, 'Meera', 'Jashmin', '123', '123', 'meera@gmail.com', '1', NULL),
(4, 'marry', 'ann', '123', '123', 'mary@gmail.com', '1', '1581655286.png'),
(5, 'lizzy', 'woo', '123', '123', 'lizzy@gmail.com', '1', '1581649908.png'),
(6, 'andrew', 'kit', '123', '123', 'andrew@gmail.com', '1', '1581662806.png'),
(7, 'raiza', 'wilson', '123', '123', 'raiza@gmail.com', '1', '1581653924.png'),
(8, 'Harish', 'Kalyan', '123', '123', 'harish@gmail.com', '1', NULL),
(9, 'Shusant', 'Singh', '123', '123', 'shusant@gmail.com', '1', NULL),
(10, 'Mugen', 'Rao', '123', '123', 'mugen@gmail.com', '1', NULL),
(11, 'Haikal', 'Fahim', '123', '123', 'haikal@gmail.com', '1', NULL),
(12, 'loisa', 'ryle', '123', '123', 'loisa@gmail.com', '1', NULL),
(13, 'admin', 'admin', '123', '123', 'admin@gmail.com', '0', NULL),
(14, 'user', 'user', '123', '123', 'user@gmail.com', '1', '1581654035.png'),
(15, 'Meera', 'Jashmin', '123', '123', 'meera@gmail.com', '1', NULL),
(16, 'marry', 'ann', '123', '123', 'mary@gmail.com', '1', '1581655286.png'),
(17, 'lizzy', 'woo', '123', '123', 'lizzy@gmail.com', '1', '1581649908.png'),
(18, 'andrew', 'kit', '123', '123', 'andrew@gmail.com', '1', '1581662806.png'),
(19, 'raiza', 'wilson', '123', '123', 'raiza@gmail.com', '1', '1581653924.png'),
(20, 'Harish', 'Kalyan', '123', '123', 'harish@gmail.com', '1', NULL),
(21, 'Shusant', 'Singh', '123', '123', 'shusant@gmail.com', '1', NULL),
(22, 'Mugen', 'Rao', '123', '123', 'mugen@gmail.com', '1', NULL),
(23, 'Haikal', 'Fahim', '123', '123', 'haikal@gmail.com', '1', NULL),
(24, 'loisa', 'ryle', '123', '123', 'loisa@gmail.com', '1', NULL),
(25, 'admin', 'admin', '123', '123', 'admin@gmail.com', '0', NULL),
(26, 'user', 'user', '123', '123', 'user@gmail.com', '1', '1581654035.png'),
(27, 'Meera', 'Jashmin', '123', '123', 'meera@gmail.com', '1', NULL),
(28, 'marry', 'ann', '123', '123', 'mary@gmail.com', '1', '1581655286.png'),
(29, 'lizzy', 'woo', '123', '123', 'lizzy@gmail.com', '1', '1581649908.png'),
(30, 'andrew', 'kit', '123', '123', 'andrew@gmail.com', '1', '1581662806.png'),
(31, 'raiza', 'wilson', '123', '123', 'raiza@gmail.com', '1', '1581653924.png'),
(32, 'Harish', 'Kalyan', '123', '123', 'harish@gmail.com', '1', NULL),
(33, 'Shusant', 'Singh', '123', '123', 'shusant@gmail.com', '1', NULL),
(34, 'Mugen', 'Rao', '123', '123', 'mugen@gmail.com', '1', NULL),
(35, 'Haikal', 'Fahim', '123', '123', 'haikal@gmail.com', '1', NULL),
(36, 'loisa', 'ryle', '123', '123', 'loisa@gmail.com', '1', NULL),
(37, 'admin', 'admin', '123', '123', 'admin@gmail.com', '0', NULL),
(38, 'user', 'user', '123', '123', 'user@gmail.com', '1', '1581654035.png'),
(39, 'Meera', 'Jashmin', '123', '123', 'meera@gmail.com', '1', NULL),
(40, 'marry', 'ann', '123', '123', 'mary@gmail.com', '1', '1581655286.png'),
(41, 'lizzy', 'woo', '123', '123', 'lizzy@gmail.com', '1', '1581649908.png'),
(42, 'andrew', 'kit', '123', '123', 'andrew@gmail.com', '1', '1581662806.png'),
(43, 'raiza', 'wilson', '123', '123', 'raiza@gmail.com', '1', '1581653924.png'),
(44, 'Harish', 'Kalyan', '123', '123', 'harish@gmail.com', '1', NULL),
(45, 'Shusant', 'Singh', '123', '123', 'shusant@gmail.com', '1', NULL),
(46, 'Mugen', 'Rao', '123', '123', 'mugen@gmail.com', '1', NULL),
(47, 'Haikal', 'Fahim', '123', '123', 'haikal@gmail.com', '1', NULL),
(48, 'loisa', 'ryle', '123', '123', 'loisa@gmail.com', '1', NULL),
(49, 'admin', 'admin', '123', '123', 'admin@gmail.com', '0', NULL),
(50, 'user', 'user', '123', '123', 'user@gmail.com', '1', '1581654035.png'),
(51, 'Meera', 'Jashmin', '123', '123', 'meera@gmail.com', '1', NULL),
(52, 'marry', 'ann', '123', '123', 'mary@gmail.com', '1', '1581655286.png'),
(53, 'lizzy', 'woo', '123', '123', 'lizzy@gmail.com', '1', '1581649908.png'),
(54, 'andrew', 'kit', '123', '123', 'andrew@gmail.com', '1', '1581662806.png'),
(55, 'raiza', 'wilson', '123', '123', 'raiza@gmail.com', '1', '1581653924.png'),
(56, 'Harish', 'Kalyan', '123', '123', 'harish@gmail.com', '1', NULL),
(57, 'Shusant', 'Singh', '123', '123', 'shusant@gmail.com', '1', NULL),
(58, 'Mugen', 'Rao', '123', '123', 'mugen@gmail.com', '1', NULL),
(59, 'Haikal', 'Fahim', '123', '123', 'haikal@gmail.com', '1', NULL),
(60, 'loisa', 'ryle', '123', '123', 'loisa@gmail.com', '1', NULL),
(61, 'admin', 'admin', '123', '123', 'admin@gmail.com', '0', NULL),
(62, 'user', 'user', '123', '123', 'user@gmail.com', '1', '1581654035.png'),
(63, 'Meera', 'Jashmin', '123', '123', 'meera@gmail.com', '1', NULL),
(64, 'marry', 'ann', '123', '123', 'mary@gmail.com', '1', '1581655286.png'),
(65, 'lizzy', 'woo', '123', '123', 'lizzy@gmail.com', '1', '1581649908.png'),
(66, 'andrew', 'kit', '123', '123', 'andrew@gmail.com', '1', '1581662806.png'),
(67, 'raiza', 'wilson', '123', '123', 'raiza@gmail.com', '1', '1581653924.png'),
(68, 'Harish', 'Kalyan', '123', '123', 'harish@gmail.com', '1', NULL),
(69, 'Shusant', 'Singh', '123', '123', 'shusant@gmail.com', '1', NULL),
(70, 'Mugen', 'Rao', '123', '123', 'mugen@gmail.com', '1', NULL),
(71, 'Haikal', 'Fahim', '123', '123', 'haikal@gmail.com', '1', NULL),
(72, 'loisa', 'ryle', '123', '123', 'loisa@gmail.com', '1', NULL),
(73, 'admin', 'admin', '123', '123', 'admin@gmail.com', '0', NULL),
(74, 'user', 'user', '123', '123', 'user@gmail.com', '1', '1581654035.png'),
(75, 'Meera', 'Jashmin', '123', '123', 'meera@gmail.com', '1', NULL),
(76, 'marry', 'ann', '123', '123', 'mary@gmail.com', '1', '1581655286.png'),
(77, 'lizzy', 'woo', '123', '123', 'lizzy@gmail.com', '1', '1581649908.png'),
(78, 'andrew', 'kit', '123', '123', 'andrew@gmail.com', '1', '1581662806.png'),
(79, 'raiza', 'wilson', '123', '123', 'raiza@gmail.com', '1', '1581653924.png'),
(80, 'Harish', 'Kalyan', '123', '123', 'harish@gmail.com', '1', NULL),
(81, 'Shusant', 'Singh', '123', '123', 'shusant@gmail.com', '1', NULL),
(82, 'Mugen', 'Rao', '123', '123', 'mugen@gmail.com', '1', NULL),
(83, 'Haikal', 'Fahim', '123', '123', 'haikal@gmail.com', '1', NULL),
(84, 'loisa', 'ryle', '123', '123', 'loisa@gmail.com', '1', NULL),
(85, 'lol', 'mi', '123', '123', 'lol@gmail.com', '1', NULL),
(86, 'Meera', 'khan', '123', '123', 'meera@gmail.com', '1', NULL),
(87, 'scsa', 'dsd', '123456', '123456', 'revathyt22@gmail.com', '1', NULL),
(88, 'ruby', 'qq', '123456', '123456', 'ruby@gmail.com', '1', NULL),
(89, 'marky', 'luiz', '123456', '123456', 'marky@gmail.com', '1', NULL),
(90, 'rafail', 'nadar', '1234567', '1234567', 'nadar@gmail.com', '1', NULL),
(91, 'kalki', 'rus', '123456', '123456', 'rus@gmail.com', '1', NULL),
(92, 'Vijay', 'Deverkonda', '1234567', '1234567', 'vijay@gmail.com', '1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `editor`
--

CREATE TABLE `editor` (
  `id` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `editor`
--

INSERT INTO `editor` (`id`, `content`, `created`) VALUES
(1, '<p>WYSIWYG editor plugin&nbsp;<s>(CKEditor or TinyMCE)</s>&nbsp;to add HTML editor to textarea input field.<br />\r\n<strong>Add WYSIWYG editor with CKEditor:</strong></p>\r\n', '2020-04-16 06:51:42'),
(2, '<p><s>Hello harry<strong> hahaha&nbsp;</strong></s></p>\r\n', '2020-04-16 06:52:22'),
(3, '<p><strong>Havoc</strong> <s>Mathan</s></p>\r\n', '2020-04-16 07:00:01');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `pid` int(255) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uploaded` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `createaccount`
--
ALTER TABLE `createaccount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `editor`
--
ALTER TABLE `editor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`pid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `createaccount`
--
ALTER TABLE `createaccount`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7810;

--
-- AUTO_INCREMENT for table `editor`
--
ALTER TABLE `editor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `pid` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
