<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Editor.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$editorDetail = getEditor($conn," WHERE id > 0");
//$customerDetails = getCustomerDetails($conn," WHERE no_of_call = 0 AND status != 'Good' LIMIT 500");

?>

<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Text Editor</title>
    <meta charset="UTF-8">
    <meta name="description">
    <?php include 'css.php'; ?>

    <script src="ckeditor/ckeditor.js"></script>
</head>

<body>

        <div class="container">
        <table class="shipping-table" id="myTable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>CONTENT</th>
                            <th>CREATED DATE</th>

                        </tr>
                    </thead>

                    <tbody>
                        <?php

                        if($editorDetail)
                        {   
                            for($cnt = 0;$cnt < count($editorDetail) ;$cnt++)
                            {?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $editorDetail[$cnt]->getId();?></td>
                                <td><?php echo $editorDetail[$cnt]->getContent();?></td>
                                <td><?php echo $editorDetail[$cnt]->getCreated();?></td>

                            <?php
                            }?>
                            </tr>
                        <?php
                        }

                        ?>
                    </tbody>

                </table>
        </div>

    <script>
        CKEDITOR.replace('editor');
    </script>
</body>
</html>