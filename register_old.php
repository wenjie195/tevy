<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$countryList = getCountries($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php //include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/" />
<meta property="og:title" content="ASEAN Investments Digital Exchange | Aidex" />
<title>ASEAN Investments Digital Exchange | Aidex</title>
<link rel="canonical" href="https://aidex.sg/" />
<?php //include 'css.php'; ?>
</head>

<body class="body">

    <div class="landing-first-div width100 overflow">
        
        <form action="utilities/newUserRegisterFunction.php" method="POST">
            <h2 class="tab-h2">Register</h2>
            <div class="input-div">
                <p class="input-top-text">Username</p>
                <input class="aidex-input clean" type="text" placeholder="Type Your Username" id="register_username" name="register_username" required>
            </div>

            <div class="input-div">
            <p class="input-top-text">Password</p>
            <input class="aidex-input clean password-input" type="password" placeholder="Password" id="register_password" name="register_password" required>
            </div>

            <div class="input-div">
            <p class="input-top-text">Retype Password</p>
            <input class="aidex-input clean password-input" type="password" placeholder="Retype Password" id="register_retype_password" name="register_retype_password" required>
            </div>

            <div class="input-div">
                <p class="input-top-text">Email</p>
                <input class="aidex-input clean" type="email" placeholder="Type Your Email" id="register_email" name="register_email" required>
            </div>

            <div class="input-div">
                <p class="input-top-text">Contact Number</p>
                <input class="aidex-input clean" type="text" placeholder="Contact Number" id="register_phone" name="register_phone" required>
            </div>

            <div class="clear"></div>

            <button class="full-width-btn blue-bg blue-btn-hover long-blue-div clean-button clean extra-margin-top" name="register">Create Account</button>

            </div>
        </form>

    </div>

<?php //include 'js.php'; ?>

</body>
</html>