<?php
class ArticlesOne {
    /* Member variables */
    var $id,$uid,$authorUid,$authorName,$title,$seoTitle,$articleLink,$keywordOne,$keywordTwo,$titleCover,$paragraphOne,$imageOne,$paragraphTwo,$imageTwo,$paragraphThree,$imageThree,
            $paragraphFour,$imageFour,$paragraphFive,$imageFive,$paragraphSix,$paragraphSeven,$paragraphEight,$paragraphNine,$paragraphTen,
                $imageSix,$imageSeven,$imageEight,$imageNine,$imageTen,$imgCoverSrc,$imgOneSrc,$imgTwoSrc,$imgThreeSrc,$imgFourSrc,$imgFiveSrc,$imgSixSrc,
                    $imgSevenSrc,$imgEightSrc,$imgNineSrc,$imgTenSrc,$uTubeLinkOne,$uTubeLinkTwo,$uTubeLinkThree,$uTubeLinkFour,$uTubeLinkFive,$uTubeLinkSix,$uTubeLinkSeven,$uTubeLinkEight,
                        $uTubeLinkNine,$uTubeLinkTen,$tubeOne,$tubeTwo,$tubeThree,$tubeFour,$tubeFive,$tubeSix,$tubeSeven,$tubeEight,$tubeNine,$tubeTen,$author,$type,$display,
                            $dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getAuthorUid()
    {
        return $this->authorUid;
    }

    /**
     * @param mixed $authorUid
     */
    public function setAuthorUid($authorUid)
    {
        $this->authorUid = $authorUid;
    }

    /**
     * @return mixed
     */
    public function getAuthorName()
    {
        return $this->authorName;
    }

    /**
     * @param mixed $authorName
     */
    public function setAuthorName($authorName)
    {
        $this->authorName = $authorName;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    /**
     * @param mixed $seoTitle
     */
    public function setSeoTitle($seoTitle)
    {
        $this->seoTitle = $seoTitle;
    }

    /**
     * @return mixed
     */
    public function getArticleLink()
    {
        return $this->articleLink;
    }

    /**
     * @param mixed $articleLink
     */
    public function setArticleLink($articleLink)
    {
        $this->articleLink = $articleLink;
    }

    /**
     * @return mixed
     */
    public function getKeywordOne()
    {
        return $this->keywordOne;
    }

    /**
     * @param mixed $keywordOne
     */
    public function setKeywordOne($keywordOne)
    {
        $this->keywordOne = $keywordOne;
    }

    /**
     * @return mixed
     */
    public function getKeywordTwo()
    {
        return $this->keywordTwo;
    }

    /**
     * @param mixed $seoTitle
     */
    public function setKeywordTwo($keywordTwo)
    {
        $this->keywordTwo = $keywordTwo;
    }

    /**
     * @return mixed
     */
    public function getTitleCover()
    {
        return $this->titleCover;
    }

    /**
     * @param mixed $titleCover
     */
    public function setTitleCover($titleCover)
    {
        $this->titleCover = $titleCover;
    }

    /**
     * @return mixed
     */
    public function getParagraphOne()
    {
        return $this->paragraphOne;
    }

    /**
     * @param mixed $paragraphOne
     */
    public function setParagraphOne($paragraphOne)
    {
        $this->paragraphOne = $paragraphOne;
    }

    /**
     * @return mixed
     */
    public function getImageOne()
    {
        return $this->imageOne;
    }

    /**
     * @param mixed $imageOne
     */
    public function setImageOne($imageOne)
    {
        $this->imageOne = $imageOne;
    }

    /**
     * @return mixed
     */
    public function getParagraphTwo()
    {
        return $this->paragraphTwo;
    }

    /**
     * @param mixed $paragraphTwo
     */
    public function setParagraphTwo($paragraphTwo)
    {
        $this->paragraphTwo = $paragraphTwo;
    }

    /**
     * @return mixed
     */
    public function getImageTwo()
    {
        return $this->imageTwo;
    }

    /**
     * @param mixed $imageTwo
     */
    public function setImageTwo($imageTwo)
    {
        $this->imageTwo = $imageTwo;
    }

    /**
     * @return mixed
     */
    public function getParagraphThree()
    {
        return $this->paragraphThree;
    }

    /**
     * @param mixed $paragraphThree
     */
    public function setParagraphThree($paragraphThree)
    {
        $this->paragraphThree = $paragraphThree;
    }

    /**
     * @return mixed
     */
    public function getImageThree()
    {
        return $this->imageThree;
    }

    /**
     * @param mixed $imageThree
     */
    public function setImageThree($imageThree)
    {
        $this->imageThree = $imageThree;
    }

    /**
     * @return mixed
     */
    public function getParagraphFour()
    {
        return $this->paragraphFour;
    }

    /**
     * @param mixed $paragraphFour
     */
    public function setParagraphFour($paragraphFour)
    {
        $this->paragraphFour = $paragraphFour;
    }

    /**
     * @return mixed
     */
    public function getImageFour()
    {
        return $this->imageFour;
    }

    /**
     * @param mixed $imageFour
     */
    public function setImageFour($imageFour)
    {
        $this->imageFour = $imageFour;
    }

    /**
     * @return mixed
     */
    public function getParagraphFive()
    {
        return $this->paragraphFive;
    }

    /**
     * @param mixed $paragraphFive
     */
    public function setParagraphFive($paragraphFive)
    {
        $this->paragraphFive = $paragraphFive;
    }

    /**
     * @return mixed
     */
    public function getImageFive()
    {
        return $this->imageFive;
    }

    /**
     * @param mixed $imageFive
     */
    public function setImageFive($imageFive)
    {
        $this->imageFive = $imageFive;
    }

    /**
     * @return mixed
     */
    public function getParagraphSix()
    {
        return $this->paragraphSix;
    }

    /**
     * @param mixed $paragraphSix
     */
    public function setParagraphSix($paragraphSix)
    {
        $this->paragraphSix = $paragraphSix;
    }

    /**
     * @return mixed
     */
    public function getParagraphSeven()
    {
        return $this->paragraphSeven;
    }

    /**
     * @param mixed $paragraphSeven
     */
    public function setParagraphSeven($paragraphSeven)
    {
        $this->paragraphSeven = $paragraphSeven;
    }

    /**
     * @return mixed
     */
    public function getParagraphEight()
    {
        return $this->paragraphEight;
    }

    /**
     * @param mixed $paragraphEight
     */
    public function setParagraphEight($paragraphEight)
    {
        $this->paragraphEight = $paragraphEight;
    }

    /**
     * @return mixed
     */
    public function getParagraphNine()
    {
        return $this->paragraphNine;
    }

    /**
     * @param mixed $paragraphNine
     */
    public function setParagraphNine($paragraphNine)
    {
        $this->paragraphNine = $paragraphNine;
    }

    /**
     * @return mixed
     */
    public function getParagraphTen()
    {
        return $this->paragraphTen;
    }

    /**
     * @param mixed $paragraphTen
     */
    public function setParagraphTen($paragraphTen)
    {
        $this->paragraphTen = $paragraphTen;
    }

    /**
     * @return mixed
     */
    public function getImageSix()
    {
        return $this->imageSix;
    }

    /**
     * @param mixed $imageSix
     */
    public function setImageSix($imageSix)
    {
        $this->imageSix = $imageSix;
    }

    /**
     * @return mixed
     */
    public function getImageSeven()
    {
        return $this->imageSeven;
    }

    /**
     * @param mixed $imageSeven
     */
    public function setImageSeven($imageSeven)
    {
        $this->imageSeven = $imageSeven;
    }

    /**
     * @return mixed
     */
    public function getImageEight()
    {
        return $this->imageEight;
    }

    /**
     * @param mixed $imageSix
     */
    public function setImageEight($imageEight)
    {
        $this->imageEight = $imageEight;
    }

    /**
     * @return mixed
     */
    public function getImageNine()
    {
        return $this->imageNine;
    }

    /**
     * @param mixed $imageSix
     */
    public function setImageNine($imageNine)
    {
        $this->imageNine = $imageNine;
    }

    /**
     * @return mixed
     */
    public function getImageTen()
    {
        return $this->imageTen;
    }

    /**
     * @param mixed $imageTen
     */
    public function setImageTen($imageTen)
    {
        $this->imageTen = $imageTen;
    }

    /**
     * @return mixed
     */
    public function getImgCoverSrc()
    {
        return $this->imgCoverSrc;
    }

    /**
     * @param mixed $imgCoverSrc
     */
    public function setImgCoverSrc($imgCoverSrc)
    {
        $this->imgCoverSrc = $imgCoverSrc;
    }

    /**
     * @return mixed
     */
    public function getImgOneSrc()
    {
        return $this->imgOneSrc;
    }

    /**
     * @param mixed $imgOneSrc
     */
    public function setImgOneSrc($imgOneSrc)
    {
        $this->imgOneSrc = $imgOneSrc;
    }

    /**
     * @return mixed
     */
    public function getImgTwoSrc()
    {
        return $this->imgTwoSrc;
    }

    /**
     * @param mixed $imgTwoSrc
     */
    public function setImgTwoSrc($imgTwoSrc)
    {
        $this->imgTwoSrc = $imgTwoSrc;
    }

    /**
     * @return mixed
     */
    public function getImgThreeSrc()
    {
        return $this->imgThreeSrc;
    }

    /**
     * @param mixed $imgThreeSrc
     */
    public function setImgThreeSrc($imgThreeSrc)
    {
        $this->imgThreeSrc = $imgThreeSrc;
    }

    /**
     * @return mixed
     */
    public function getImgFourSrc()
    {
        return $this->imgFourSrc;
    }

    /**
     * @param mixed $imgFourSrc
     */
    public function setImgFourSrc($imgFourSrc)
    {
        $this->imgFourSrc = $imgFourSrc;
    }

    /**
     * @return mixed
     */
    public function getImgFiveSrc()
    {
        return $this->imgFiveSrc;
    }

    /**
     * @param mixed $imgFiveSrc
     */
    public function setImgFiveSrc($imgFiveSrc)
    {
        $this->imgFiveSrc = $imgFiveSrc;
    }

    /**
     * @return mixed
     */
    public function getImgSixSrc()
    {
        return $this->imgSixSrc;
    }

    /**
     * @param mixed $imgSixSrc
     */
    public function setImgSixSrc($imgSixSrc)
    {
        $this->imgSixSrc = $imgSixSrc;
    }

    /**
     * @return mixed
     */
    public function getImgSevenSrc()
    {
        return $this->imgSevenSrc;
    }

    /**
     * @param mixed $imgSevenSrc
     */
    public function setImgSevenSrc($imgSevenSrc)
    {
        $this->imgSevenSrc = $imgSevenSrc;
    }

    /**
     * @return mixed
     */
    public function getImgEightSrc()
    {
        return $this->imgEightSrc;
    }

    /**
     * @param mixed $imgEightSrc
     */
    public function setImgEightSrc($imgEightSrc)
    {
        $this->imgEightSrc = $imgEightSrc;
    }

    /**
     * @return mixed
     */
    public function getImgNineSrc()
    {
        return $this->imgNineSrc;
    }

    /**
     * @param mixed $imgNineSrc
     */
    public function setImgNineSrc($imgNineSrc)
    {
        $this->imgNineSrc = $imgNineSrc;
    }

    /**
     * @return mixed
     */
    public function getImgTenSrc()
    {
        return $this->imgTenSrc;
    }

    /**
     * @param mixed $imgTenSrc
     */
    public function setImgTenSrc($imgTenSrc)
    {
        $this->imgTenSrc = $imgTenSrc;
    }

    /**
     * @return mixed
     */
    public function getUtubeLinkOne()
    {
        return $this->uTubeLinkOne;
    }

    /**
     * @param mixed $uTubeLinkOne
     */
    public function setUtubeLinkOne($uTubeLinkOne)
    {
        $this->uTubeLinkOne = $uTubeLinkOne;
    }

    /**
     * @return mixed
     */
    public function getUtubeLinkTwo()
    {
        return $this->uTubeLinkTwo;
    }

    /**
     * @param mixed $uTubeLinkTwo
     */
    public function setUtubeLinkTwo($uTubeLinkTwo)
    {
        $this->uTubeLinkTwo = $uTubeLinkTwo;
    }

    /**
     * @return mixed
     */
    public function getUtubeLinkThree()
    {
        return $this->uTubeLinkThree;
    }

    /**
     * @param mixed $uTubeLinkThree
     */
    public function setUtubeLinkThree($uTubeLinkThree)
    {
        $this->uTubeLinkThree = $uTubeLinkThree;
    }

    /**
     * @return mixed
     */
    public function getUtubeLinkFour()
    {
        return $this->uTubeLinkFour;
    }

    /**
     * @param mixed $uTubeLinkFour
     */
    public function setUtubeLinkFour($uTubeLinkFour)
    {
        $this->uTubeLinkFour = $uTubeLinkFour;
    }

    /**
     * @return mixed
     */
    public function getUtubeLinkFive()
    {
        return $this->uTubeLinkFive;
    }

    /**
     * @param mixed $uTubeLinkFive
     */
    public function setUtubeLinkFive($uTubeLinkFive)
    {
        $this->uTubeLinkFive = $uTubeLinkFive;
    }

    /**
     * @return mixed
     */
    public function getUtubeLinkSix()
    {
        return $this->uTubeLinkSix;
    }

    /**
     * @param mixed $uTubeLinkSix
     */
    public function setUtubeLinkSix($uTubeLinkSix)
    {
        $this->uTubeLinkSix = $uTubeLinkSix;
    }

    /**
     * @return mixed
     */
    public function getUtubeLinkSeven()
    {
        return $this->uTubeLinkSeven;
    }

    /**
     * @param mixed $uTubeLinkSeven
     */
    public function setUtubeLinkSeven($uTubeLinkSeven)
    {
        $this->uTubeLinkSeven = $uTubeLinkSeven;
    }

    /**
     * @return mixed
     */
    public function getUtubeLinkEight()
    {
        return $this->uTubeLinkEight;
    }

    /**
     * @param mixed $uTubeLinkEight
     */
    public function setUtubeLinkEight($uTubeLinkEight)
    {
        $this->uTubeLinkEight = $uTubeLinkEight;
    }

    /**
     * @return mixed
     */
    public function getUtubeLinkNine()
    {
        return $this->uTubeLinkNine;
    }

    /**
     * @param mixed $uTubeLinkNine
     */
    public function setUtubeLinkNine($uTubeLinkNine)
    {
        $this->uTubeLinkNine = $uTubeLinkNine;
    }

    /**
     * @return mixed
     */
    public function getUtubeLinkTen()
    {
        return $this->uTubeLinkTen;
    }

    /**
     * @param mixed $uTubeLinkTen
     */
    public function setUtubeLinkTen($uTubeLinkTen)
    {
        $this->uTubeLinkTen = $uTubeLinkTen;
    }

    /**
     * @return mixed
     */
    public function getTubeOne()
    {
        return $this->tubeOne;
    }

    /**
     * @param mixed $tubeOne
     */
    public function setTubeOne($tubeOne)
    {
        $this->tubeOne = $tubeOne;
    }

    /**
     * @return mixed
     */
    public function getTubeTwo()
    {
        return $this->tubeTwo;
    }

    /**
     * @param mixed $tubeTwo
     */
    public function setTubeTwo($tubeTwo)
    {
        $this->tubeTwo = $tubeTwo;
    }

    /**
     * @return mixed
     */
    public function getTubeThree()
    {
        return $this->tubeThree;
    }

    /**
     * @param mixed $tubeThree
     */
    public function setTubeThree($tubeThree)
    {
        $this->tubeThree = $tubeThree;
    }

    /**
     * @return mixed
     */
    public function getTubeFour()
    {
        return $this->tubeFour;
    }

    /**
     * @param mixed $tubeFour
     */
    public function setTubeFour($tubeFour)
    {
        $this->tubeFour = $tubeFour;
    }

    /**
     * @return mixed
     */
    public function getTubeFive()
    {
        return $this->tubeFive;
    }

    /**
     * @param mixed $tubeFive
     */
    public function setTubeFive($tubeFive)
    {
        $this->tubeFive = $tubeFive;
    }

    /**
     * @return mixed
     */
    public function getTubeSix()
    {
        return $this->tubeSix;
    }

    /**
     * @param mixed $tubeSix
     */
    public function setTubeSix($tubeSix)
    {
        $this->tubeSix = $tubeSix;
    }

    /**
     * @return mixed
     */
    public function getTubeSeven()
    {
        return $this->tubeSeven;
    }

    /**
     * @param mixed $tubeSeven
     */
    public function setTubeSeven($tubeSeven)
    {
        $this->tubeSeven = $tubeSeven;
    }

    /**
     * @return mixed
     */
    public function getTubeEight()
    {
        return $this->tubeEight;
    }

    /**
     * @param mixed $tubeEight
     */
    public function setTubeEight($tubeEight)
    {
        $this->tubeEight = $tubeEight;
    }

    /**
     * @return mixed
     */
    public function getTubeNine()
    {
        return $this->tubeNine;
    }

    /**
     * @param mixed $tubeNine
     */
    public function setTubeNine($tubeNine)
    {
        $this->tubeNine = $tubeNine;
    }

    /**
     * @return mixed
     */
    public function getTubeTen()
    {
        return $this->tubeTen;
    }

    /**
     * @param mixed $tubeTen
     */
    public function setTubeTen($tubeTen)
    {
        $this->tubeTen = $tubeTen;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * @param mixed $display
     */
    public function setDisplay($display)
    {
        $this->display = $display;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getArticlesOne($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","author_uid","author_name","title","seo_title","article_link","keyword_one","keyword_two","title_cover","paragraph_one","image_one","paragraph_two","image_two",
                                "paragraph_three","image_three","paragraph_four","image_four","paragraph_five","image_five","paragraph_six","paragraph_seven","paragraph_eight",
                                    "paragraph_nine","paragraph_ten","image_six","image_seven","image_eight","image_nine","image_ten","img_cover_source",
                                    "img_one_source","img_two_source","img_three_source","img_four_source","img_five_source","img_six_source","img_seven_source","img_eight_source",
                                        "img_nine_source","img_ten_source","utube_linkone","utube_linktwo","utube_linkthree","utube_linkfour","utube_linkfive","utube_linksix","utube_linkseven",
                                            "utube_linkeight","utube_linknine","utube_linkten","tube_one","tube_two","tube_three","tube_four","tube_five","tube_six","tube_seven","tube_eight","tube_nine",
                                                "tube_ten","author","type","display","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"articles_one");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$authorUid,$authorName,$title,$seoTitle,$articleLink,$keywordOne,$keywordTwo,$titleCover,$paragraphOne,$imageOne,$paragraphTwo,$imageTwo,$paragraphThree,$imageThree,
                                $paragraphFour,$imageFour,$paragraphFive,$imageFive,$paragraphSix,$paragraphSeven,$paragraphEight,$paragraphNine,$paragraphTen,
                                    $imageSix,$imageSeven,$imageEight,$imageNine,$imageTen,$imgCoverSrc,$imgOneSrc,$imgTwoSrc,$imgThreeSrc,$imgFourSrc,$imgFiveSrc,$imgSixSrc,
                                        $imgSevenSrc,$imgEightSrc,$imgNineSrc,$imgTenSrc,$uTubeLinkOne,$uTubeLinkTwo,$uTubeLinkThree,$uTubeLinkFour,$uTubeLinkFive,$uTubeLinkSix,$uTubeLinkSeven,$uTubeLinkEight,
                                            $uTubeLinkNine,$uTubeLinkTen,$tubeOne,$tubeTwo,$tubeThree,$tubeFour,$tubeFive,$tubeSix,$tubeSeven,$tubeEight,$tubeNine,$tubeTen,$author,$type,$display,
                                                $dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new ArticlesOne;
            $class->setId($id);
            $class->setUid($uid);

            $class->setAuthorUid($authorUid);
            $class->setAuthorName($authorName);
            $class->setTitle($title);

            $class->setSeoTitle($seoTitle);

            $class->setArticleLink($articleLink);

            $class->setKeywordOne($keywordOne);
            $class->setKeywordTwo($keywordTwo);

            $class->setTitleCover($titleCover);

            $class->setParagraphOne($paragraphOne);
            $class->setImageOne($imageOne);
            $class->setParagraphTwo($paragraphTwo);
            $class->setImageTwo($imageTwo);

            $class->setParagraphThree($paragraphThree);
            $class->setImageThree($imageThree);
            $class->setParagraphFour($paragraphFour);
            $class->setImageFour($imageFour);
            $class->setParagraphFive($paragraphFive);
            $class->setImageFive($imageFive);

            $class->setParagraphSix($paragraphSix);
            $class->setParagraphSeven($paragraphSeven);
            $class->setParagraphEight($paragraphEight);

            $class->setParagraphNine($paragraphNine);
            $class->setParagraphTen($paragraphTen);
            $class->setImageSix($imageSix);
            $class->setImageSeven($imageSeven);
            $class->setImageEight($imageEight);
            $class->setImageNine($imageNine);
            $class->setImageTen($imageTen);

            $class->setImgCoverSrc($imgCoverSrc);

            $class->setImgOneSrc($imgOneSrc);
            $class->setImgTwoSrc($imgTwoSrc);
            $class->setImgThreeSrc($imgThreeSrc);
            $class->setImgFourSrc($imgFourSrc);
            $class->setImgFiveSrc($imgFiveSrc);
            $class->setImgSixSrc($imgSixSrc);
            $class->setImgSevenSrc($imgSevenSrc);
            $class->setImgEightSrc($imgEightSrc);
            $class->setImgNineSrc($imgNineSrc);
            $class->setImgTenSrc($imgTenSrc);

            $class->setUtubeLinkOne($uTubeLinkOne);
            $class->setUtubeLinkTwo($uTubeLinkTwo);
            $class->setUtubeLinkThree($uTubeLinkThree);
            $class->setUtubeLinkFour($uTubeLinkFour);
            $class->setUtubeLinkFive($uTubeLinkFive);
            $class->setUtubeLinkSix($uTubeLinkSix);
            $class->setUtubeLinkSeven($uTubeLinkSeven);
            $class->setUtubeLinkEight($uTubeLinkEight);
            $class->setUtubeLinkNine($uTubeLinkNine);
            $class->setUtubeLinkTen($uTubeLinkTen);

            $class->setTubeOne($tubeOne);
            $class->setTubeTwo($tubeTwo);
            $class->setTubeThree($tubeThree);
            $class->setTubeFour($tubeFour);
            $class->setTubeFive($tubeFive);
            $class->setTubeSix($tubeSix);
            $class->setTubeSeven($tubeSeven);
            $class->setTubeEight($tubeEight);
            $class->setTubeNine($tubeNine);
            $class->setTubeTen($tubeTen);

            $class->setAuthor($author);

            $class->setType($type);
            $class->setDisplay($display);

            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
