<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/ArticleOne.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

// $timestamp = time();

function uploadNewArticles($conn,$uid,$title,$titleCover,$paragraphOne,$type)
{
     if(insertDynamicData($conn,"articles_one",array("uid","title","title_cover","paragraph_one","type"),
          array($uid,$title,$titleCover,$paragraphOne,$type),"sssss") === null)
     {
          header('Location: ../userUploadArticles.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     // $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

     // $authorUid = $userDetails[0]->getUid();
     // $authorName = $userDetails[0]->getUsername();

     $title = rewrite($_POST['title']);

     $titleCover = $_FILES['cover_photo']['name'];
     // $titleCover = $timestamp.$_FILES['cover_photo']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["cover_photo"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['cover_photo']['tmp_name'],$target_dir.$titleCover);
     }

     $paragraphOne = rewrite($_POST['long_desc']);

     $type = rewrite($_POST['type']);

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $title."<br>";
     // echo $paragraphOne."<br>";
     // echo $type."<br>";

     if(uploadNewArticles($conn,$uid,$title,$titleCover,$paragraphOne,$type))
     {
          // echo "articles upload successfully";
          // echo "<script>alert('Register Success !');window.location='../addReferee.php'</script>";    

          if($type == 'Beauty')
          {
               header('Location: ../beautyCare.php');
          }
          elseif($type == 'Fashion')
          {
               header('Location: ../trendyFashion.php');
          }
          elseif($type == 'Social')
          {
               header('Location: ../socialNews.php');
          }
          else
          {
               echo "error";
          }

     }
     else
     {
          echo "fail to upload article";
     }
  
}
else 
{
     header('Location: ../index.php');
}

?>