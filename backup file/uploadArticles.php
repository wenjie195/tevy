<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid  = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:image" content="https://tevy.asia/img/fb-meta.jpg" />
<meta property="og:title" content="Upload Article | Tevy" />
<meta property="og:description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="keywords" content="Tevy, girls, female, lady, ladies, news, beauty care, beauty, skin care, fashion, social, etc">

<link rel="canonical" href="https://tevy.asia/userUploadArticles.php" />
<title>Upload Article | Tevy</title>

<style type="text/css">
    .cke_textarea_inline
    {
        border: 1px solid black;
    }
</style>

<!-- CKEditor -->	
<script src="ckeditor/ckeditor.js" ></script>

</head>

<body>

	<!-- <form method='post' action=''> -->
	<form action="utilities/addNewArticlesNewFunction.php" method="POST" enctype="multipart/form-data">
		Title : 
		<input type="text" name="title" ><br>

		Short Description: 
		<textarea id='short_desc' name='short_desc' style='border: 1px solid black;'  ></textarea><br>

		Long Description: 
		<textarea id='long_desc' name='long_desc' ></textarea><br>

		<div class="form-group">
			<h4 class="title-color"><b class="family">Category</b></h4>
			<select class="clean aidex-input" type="text" id="type" name="type" required>
				<option value="" name=" "><?php echo _UPLOAD_ARTICLE_CHOOSE_A_CATEGORY ?></option>
				<option value="Beauty" name="Beauty"><?php echo _HEADER_BEAUTY ?></option>
				<option value="Fashion" name="Fashion"><?php echo _HEADER_FASHION ?></option>
				<option value="Social" name="Social"><?php echo _HEADER_SOCIAL ?></option>
			</select>    
		</div>

		<div class="input-div">
			<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO ?></p>
			<input id="file-upload" type="file" name="cover_photo" id="cover_photo" accept="image/*" required>    
		</div>   

		<!-- <input type="submit" name="submit" value="Submit"> -->

		<div class="mobile publish-border">
			<div class="pull-right">
				<button class="clean-button clean login-btn pink-button" name="submit"><?php echo _UPLOAD_ARTICLE_SUBMIT ?></button>
			</div>
		</div>

	</form>
	
	<!-- Script -->
	<script type="text/javascript">
	
		// Initialize CKEditor
		CKEDITOR.inline( 'short_desc');

		CKEDITOR.replace('long_desc',{

			width: "500px",
        	height: "200px"
   
		}); 
	
    	
	</script>
</body>
</html>          