<?php
//for main.js modal files
define("_MAINJS_ATTENTION", "Attention");
define("_MAINJS_ENTER_USERNAME", "Please enter your username");
define("_MAINJS_ENTER_EMAIL", "Please enter your email address");
define("_MAINJS_ENTER_ICNO", "Please enter your ID number");
define("_MAINJS_SELECT_COUNTRY", "Please choose your country");
define("_MAINJS_ENTER_PHONENO", "Please enter your phone number");
//apply in all
define("_MAINJS_ALL_LOGOUT", "Logout");
//index
define("_MAINJS_INDEX_LOGIN", "Login");
define("_MAINJS_INDEX_USERNAME", "Username");
define("_MAINJS_INDEX_PASSWORD", "Password");
define("_MAINJS_INDEX_LATEST_ART", "Latest Articles");
//header
define("_HEADER_LANGUAGE", "Language/语言");
define("_HEADER_LOGOUT", "Logout");
define("_HEADER_PROFILE", "Profile");
define("_HEADER_EDIT_PROFILE", "Edit Profile");
define("_HEADER_CHANGE_EMAIL", "Change Email");
define("_HEADER_CHANGE_PHONE_NO", "Change Phone No.");
define("_HEADER_CHANGE_PASSWORD", "Change Password");
define("_HEADER_SIGN_UP", "Sign Up");
define("_HEADER_LOGIN", "Login");
define("_HEADER_MESSAGE", "Message");
define("_HEADER_HOME", "Home");
define("_HEADER_BEAUTY", "Beauty");
define("_HEADER_FASHION", "Fashion");
define("_HEADER_SOCIAL", "Social");
define("_HEADER_ROMANCE", "Romance/Love Life");
define("_HEADER_ROMANCE_MENU", "Romance");
define("_HEADER_ENTREPRENEURSHIP", "Entrepreneurship");
define("_HEADER_DIY", "DIY");
define("_HEADER_COOKING", "Cooking");
define("_HEADER_ESPORT", "E-Sports/Games/Entertainment");
define("_HEADER_ESPORT_MENU", "E-Sports");
define("_HEADER_LIFESTYLE", "Lifestyle");
define("_HEADER_PETS", "Pets");
define("_HEADER_TRAVEL", "Travel");
define("_HEADER_ARTICLE", "Article");
define("_HEADER_UPLOAD_ARTICLE", "Upload Article");
define("_HEADER_EDIT_ARTICLE", "Edit Article");
//JS
define("_JS_LOGIN", "Login");
define("_JS_USERNAME", "Username");
define("_JS_PASSWORD", "Password");
define("_JS_FULLNAME", "Fullname");
define("_JS_NEW_PASSWORD", "New Password");
define("_JS_CURRENT_PASSWORD", "Current Password");
define("_JS_RETYPE_PASSWORD", "Retype Password");
define("_JS_REMEMBER_ME", "Remember Me");
define("_JS_FORGOT_PASSWORD", "Forgot Password?");
define("_JS_FORGOT_TITLE", "Forgot Password");
define("_JS_EMAIL", "Email");
define("_JS_SIGNUP", "Sign Up");
define("_JS_FIRSTNAME", "First Name");
define("_JS_LASTNAME", "Last Name");
define("_JS_GENDER", "Gender");
define("_JS_MALE", "Male");
define("_JS_FEMALE", "Female");
define("_JS_BIRTHDAY", "Birthday");
define("_JS_COUNTRY", "Country");
define("_JS_CLOSE", "Close");
define("_JS_ERROR", "Error");
define("_JS_CONTACT_NO", "Contact No.");
//VIEW MESSAGE
define("_VIEWMESSAGE_VIEW_ALL_MESSAGE", "View All Message");
define("_VIEWMESSAGE_NO", "NO.");
define("_VIEWMESSAGE_SENT", "SENT");
define("_VIEWMESSAGE_REPLY", "REPLY");
define("_VIEWMESSAGE_DATE", "DATE");
define("_VIEWMESSAGE_MESSAGE_STATUS", "MESSAGE STATUS");
define("_VIEWMESSAGE_READ", "READ");
define("_VIEWMESSAGE_NEW_MESSAGE", "New Message");
define("_VIEWMESSAGE_CHOOSE_YOUR_FILE", "Choose Your File");
//Profile
define("_PROFILE_PERSONAL_DETAILS", "Personal Details");
define("_PROFILE_CHOOSE_COUNTRY", "Choose Country");
//Top Up History
define("_TOPUP_HISTORY_DATE", "Date");
//ViewMessage
define("_VIEWMESSAGE_SENT2", "Send");
define("_VIEWMESSAGE_UR_MESSAGE", "Your Message Here");
define("_VIEWMESSAGE_UPLOAD", "Upload Image");
define("_VIEWMESSAGE_JUST_UPLOAD", "Upload");
define("_VIEWMESSAGE_SENT3", "Send");
//Article
define("_ARTICLE_SHARE", "Share");
define("_ARTICLE_RECOMMENDED", "Recommended");
define("_ARTICLE_SOURCE", "Image Source/Credit");
//Upload Article
define("_UPLOAD_ARTICLE_NEW", "New Article");
define("_UPLOAD_ARTICLE_TITLE", "Title");
define("_UPLOAD_ARTICLE_LINK", "Article Link (English only, no Spacing or any symbol like coma and dot, can use - to separate the word)");
define("_UPLOAD_ARTICLE_EXAMPLE", "Example");
define("_UPLOAD_ARTICLE_GOOGLE_KEYWORD", "Google Search Keyword (Use coma to separate the keyword)");
define("_UPLOAD_ARTICLE_CATEGORY", "Category");
define("_UPLOAD_ARTICLE_CHOOSE_A_CATEGORY", "Choose a Category");
define("_UPLOAD_ARTICLE_COVER_PHOTO", "Cover Photo");
define("_UPLOAD_ARTICLE_PARAGRAPH", "Paragraph");
define("_UPLOAD_ARTICLE_IMAGE", "Image");
define("_UPLOAD_ARTICLE_SUBMIT", "Submit");
define("_UPLOAD_ARTICLE_AUTHOR", "Author");
define("_UPLOAD_ARTICLE_ADD_MORE_PARAGRAPH", "Add More Paragraph");
define("_UPLOAD_ARTICLE_COVER_PHOTO_SOURCE", "Source of Photo");
define("_UPLOAD_ARTICLE_YOUTUBE_VIDEO", "Youtube Video ID");
define("_UPLOAD_ARTICLE_YOUTUBE_VIDEO_LINK", "Youtube Video ID, Please Refer to the ! Icon Above");
define("_UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE", "How to Copy Youtube Video ID?");
define("_UPLOAD_ARTICLE_YOUTUBE_ID_STEP1", "Desktop View Step 1: Click Share");
define("_UPLOAD_ARTICLE_YOUTUBE_ID_STEP2", "Desktop View Step 2: Copy the ID Underline in Red, the ID after https://youtu.be/");
define("_UPLOAD_ARTICLE_YOUTUBE_ID_ALTERNATE_STEP", "Alternate Step (Work for both Desktop and Mobile View): Copy the ID after https://youtube.com/watch?v=/");
define("_UPLOAD_ARTICLE_YOUTUBE_APP_STEP1", "Youtube App Step 1: Click Share");
define("_UPLOAD_ARTICLE_YOUTUBE_APP_STEP2", "Youtube App Step 2: Click Copy Link");
define("_UPLOAD_ARTICLE_YOUTUBE_APP_STEP3", "Youtube App Step 3: Paste the Copied Link and Remove the https://youtu.be/");
define("_UPLOAD_ARTICLE_YOUTUBE_APP_STEP4", "Youtube App Step 4: Keep the ID only");
define("_UPLOAD_ARTICLE_CLOSE", "Close");
define("_UPLOAD_ARTICLE_REFER_ARTICLE_LINK", "Article Link Sample");
define("_UPLOAD_ARTICLE_DESCRIPTION", "Summary");
define("_UPLOAD_ARTICLE_BODY_TEXT", "Article Content");
define("_UPLOAD_ARTICLE_REFER_GOOGLE_KEYWORD", "What is Google Keyword?");
define("_UPLOAD_ARTICLE_REFER_DESCRIPTION", "Where Article Summary Will Be Shown?");
define("_UPLOAD_ARTICLE_WHAT_IS_ARTICLE_LINK", "What is Article Link/URL?");
define("_UPLOAD_ARTICLE_WHAT_IS_ARTICLE_LINK_EXP", "Article link/URL is the link to your article. It shall not repeat with other article or contain any spacing or symbol such as coma and dot , 'etc. You can use - to separate the text. The more popular and hot keyword used to form the link will help your article get better ranking in Google search result page. ");
define("_UPLOAD_ARTICLE_WHAT_IS_KEYWORD", "What is Google Keyword?");
define("_UPLOAD_ARTICLE_WHAT_IS_KEYWORD_EXP", "Google Keyword is the keyword when the user typed to search for the content in Google Search. Google will show the website contains the keyword on top of the search result. You can help user to find this article easier if you key in related keyword. This keyword is only helping the system, it won't appear inside the article or anywhere to be seen. You need to use a coma , to separate each keyword. Example: eating, habit, healthy, vegetable, ");
define("_UPLOAD_ARTICLE_WHAT_IS_DESCRIPTION", "Where Article Summary Will Be Shown?");
define("_UPLOAD_ARTICLE_WHAT_IS_DESCRIPTION_EXP", "Article summary will be shown at the homepage as short description of the article and inside the Google Search Result. It won't show inside the article content.");
define("_HEADER_UPLOAD_ARTICLE_NEW", "NEW");
define("_UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE", "How to Embed Youtube Video?");
define("_UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE1", "Click iframe (the Earth icon) from the tool list.");
define("_UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE2", "Click share below the Youtube video.");
define("_UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE3", "Click embed.");
define("_UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE4", "Copy the link ONLY, start from https");
define("_UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE5", "Paste the link and click ok.");
define("_UPLOAD_ARTICLE_HOW_TO_LINK", "How to insert link? Click the link/chain icon.");
define("_UPLOAD_ARTICLE_HOW_TO_LINK2", "Type the display text for the link. Paste the link inside URL column.");
define("_UPLOAD_ARTICLE_HOW_TO_PLACE_IMAGE", "How to place image? Click the image icon. Please don't directly copy paste image inside.");
define("_UPLOAD_ARTICLE_HOW_TO_PLACE_IMAGE2", "Right click the image and choose copy image address.");
define("_UPLOAD_ARTICLE_HOW_TO_PLACE_IMAGE3", "Paste the image address inside and click ok.");
define("_UPLOAD_ARTICLE_BODY_TEXT_FAQ", "Important! Don't insert “” and ‘’! Upload image according to the tutorial! Click the icon.");
define("_UPLOAD_ARTICLE_HOW_TO_EMBED_FB_VIDEO", "How to Embed Facebook Video?");
define("_UPLOAD_ARTICLE_HOW_TO_EMBED_FB_VIDEO1", "Make sure the video settings is public. Click share and then click embed.");
define("_UPLOAD_ARTICLE_HOW_TO_EMBED_FB_VIDEO2", "Copy the link inside the after src (the highlighted part inside the picture). ");
//View Article
define("_VIEW_ARTICLES_ALL", "All Articles");
define("_VIEW_ARTICLES_DATE", "Date");
define("_VIEW_ARTICLES_AUTHOR", "Author");
define("_VIEW_ARTICLES_EDIT", "Edit");
define("_VIEW_ARTICLES_DELETE", "Delete");
define("_VIEW_ARTICLES_SHOW_HIDE", "Delete");
define("_VIEW_ARTICLES_SHOW", "Undo");
define("_VIEW_ARTICLES_HIDE", "Delete");
//Edit Article
define("_EDIT_ARTICLE", "Edit Article");
define("_EDIT_ARTICLE_SUBMIT", "Submit");
//Footer
define("_FOOTER_ALL_RIGHT", "Tevy, All Right Reserved");
define("_FOOTER_CREATE_ACCOUNT", "Create Account");