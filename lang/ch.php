<?php
//for main.js modal files
define("_MAINJS_ATTENTION", "注意");
define("_MAINJS_ENTER_USERNAME", "请输入您的用户名");
define("_MAINJS_ENTER_EMAIL", "请输入您的电子邮件地址");
define("_MAINJS_ENTER_ICNO", "请输入您的身份证号码");
define("_MAINJS_SELECT_COUNTRY", "请选择你的国家");
define("_MAINJS_ENTER_PHONENO", "请输入您的电话号码");
//apply in all
define("_MAINJS_ALL_LOGOUT", "登出");
//index
define("_MAINJS_INDEX_LOGIN", "登录");
define("_MAINJS_INDEX_USERNAME", "用户名");
define("_MAINJS_INDEX_PASSWORD", "密码");
define("_MAINJS_INDEX_LATEST_ART", "最新资讯");
//header
define("_HEADER_LANGUAGE", "Language/语言");
define("_HEADER_LOGOUT", "登出");
define("_HEADER_PROFILE", "个人中心");
define("_HEADER_EDIT_PROFILE", "修改个人资料");
define("_HEADER_CHANGE_EMAIL", "更改电邮地址");
define("_HEADER_CHANGE_PHONE_NO", "更改手机号码");
define("_HEADER_CHANGE_PASSWORD", "更改密码");
define("_HEADER_SIGN_UP", "注册");
define("_HEADER_LOGIN", "登录");
define("_HEADER_MESSAGE", "信息");
define("_HEADER_HOME", "主页");
define("_HEADER_BEAUTY", "美容");
define("_HEADER_FASHION", "时尚");
define("_HEADER_SOCIAL", "社交");
define("_HEADER_ROMANCE", "浪漫");
define("_HEADER_ROMANCE_MENU", "浪漫");
define("_HEADER_ENTREPRENEURSHIP", "创业");
define("_HEADER_DIY", "自制");
define("_HEADER_COOKING", "烹饪");
define("_HEADER_ESPORT", "电游");
define("_HEADER_ESPORT_MENU", "电游");
define("_HEADER_LIFESTYLE", "生活");
define("_HEADER_PETS", "宠物");
define("_HEADER_TRAVEL", "旅游");
define("_HEADER_ARTICLE", "文章");
define("_HEADER_UPLOAD_ARTICLE", "上载文章");
define("_HEADER_EDIT_ARTICLE", "修改文章");
//JS
define("_JS_LOGIN", "登入");
define("_JS_USERNAME", "用户名");
define("_JS_PASSWORD", "密码");
define("_JS_FULLNAME", "全名");
define("_JS_NEW_PASSWORD", "新密码");
define("_JS_CURRENT_PASSWORD", "现在的密码");
define("_JS_RETYPE_PASSWORD", "再次输入密码");
define("_JS_REMEMBER_ME", "记住我");
define("_JS_FORGOT_PASSWORD", "忘记密码");
define("_JS_FORGOT_TITLE", "忘记密码");
define("_JS_EMAIL", "邮箱地址");
define("_JS_SIGNUP", "注册");
define("_JS_FIRSTNAME", "名字");
define("_JS_LASTNAME", "姓氏");
define("_JS_GENDER", "性别");
define("_JS_MALE", "男");
define("_JS_FEMALE", "女");
define("_JS_BIRTHDAY", "出生日期");
define("_JS_COUNTRY", "国家");
define("_JS_CLOSE", "关闭");
define("_JS_ERROR", "错误");
define("_JS_CONTACT_NO", "联络号码");
//VIEW MESSAGE
define("_VIEWMESSAGE_VIEW_ALL_MESSAGE", "浏览所有信息");
define("_VIEWMESSAGE_NO", "序");
define("_VIEWMESSAGE_SENT", "已发出");
define("_VIEWMESSAGE_REPLY", "回复");
define("_VIEWMESSAGE_DATE", "日期");
define("_VIEWMESSAGE_MESSAGE_STATUS", "状态");
define("_VIEWMESSAGE_READ", "已读");
define("_VIEWMESSAGE_NEW_MESSAGE", "新信息");
define("_VIEWMESSAGE_CHOOSE_YOUR_FILE", "选文件上传");
//Profile
define("_PROFILE_PERSONAL_DETAILS", "个人资料");
define("_PROFILE_CHOOSE_COUNTRY", "选择国家");
//Top Up History
define("_TOPUP_HISTORY_DATE", "日期");
//ViewMessage
define("_VIEWMESSAGE_SENT2", "发出");
define("_VIEWMESSAGE_UR_MESSAGE", "输入你的信息");
define("_VIEWMESSAGE_UPLOAD", "上载照片");
define("_VIEWMESSAGE_JUST_UPLOAD", "上载");
define("_VIEWMESSAGE_SENT3", "上载");
//Article
define("_ARTICLE_SHARE", "分享");
define("_ARTICLE_RECOMMENDED", "你可能喜欢");
define("_ARTICLE_SOURCE", "照片出处");
//Upload Article
define("_UPLOAD_ARTICLE_NEW", "新文章");
define("_UPLOAD_ARTICLE_TITLE", "标题");
define("_UPLOAD_ARTICLE_LINK", "文章链接 （只能使用英文字母和-,不可以有空格和任何符号包括逗号、开关引号和句号,可用-代替空格）");
define("_UPLOAD_ARTICLE_EXAMPLE", "例如");
define("_UPLOAD_ARTICLE_GOOGLE_KEYWORD", "谷歌搜索关键词 (请在每个关键词后面加上英文输入法逗号,)");
define("_UPLOAD_ARTICLE_CATEGORY", "分类");
define("_UPLOAD_ARTICLE_CHOOSE_A_CATEGORY", "选择类别");
define("_UPLOAD_ARTICLE_COVER_PHOTO", "封面图");
define("_UPLOAD_ARTICLE_PARAGRAPH", "文章段落");
define("_UPLOAD_ARTICLE_IMAGE", "照片");
define("_UPLOAD_ARTICLE_SUBMIT", "上载");
define("_UPLOAD_ARTICLE_AUTHOR", "作者");
define("_UPLOAD_ARTICLE_ADD_MORE_PARAGRAPH", "增加段落");
define("_UPLOAD_ARTICLE_COVER_PHOTO_SOURCE", "照片出处");
define("_UPLOAD_ARTICLE_YOUTUBE_VIDEO", "Youtube视频ID");
define("_UPLOAD_ARTICLE_YOUTUBE_VIDEO_LINK", "Youtube视频ID，请点击上面!查询");
define("_UPLOAD_ARTICLE_HOW_TO_COPY_YOUTUBE", "如何复制Youtube ID?");
define("_UPLOAD_ARTICLE_YOUTUBE_ID_STEP1", "电脑版步骤 1: 点击Share/分享");
define("_UPLOAD_ARTICLE_YOUTUBE_ID_STEP2", "电脑版步骤 2: 复制https://youtu.be/之后标注红线的ID,别复制https://youtu.be/");
define("_UPLOAD_ARTICLE_YOUTUBE_ID_ALTERNATE_STEP", "其他方式（电脑或手机版皆可用）: 复制https://youtube.com/watch?v=/之后的ID");
define("_UPLOAD_ARTICLE_YOUTUBE_APP_STEP1", "Youtube App步骤 1: 点击Share/分享");
define("_UPLOAD_ARTICLE_YOUTUBE_APP_STEP2", "Youtube App步骤 2: 点击Copy Link/复制链接");
define("_UPLOAD_ARTICLE_YOUTUBE_APP_STEP3", "Youtube App步骤 3: 粘贴已复制的链接后删除https://youtu.be/");
define("_UPLOAD_ARTICLE_YOUTUBE_APP_STEP4", "Youtube App步骤 4: 只留下ID");
define("_UPLOAD_ARTICLE_CLOSE", "关闭");
define("_UPLOAD_ARTICLE_REFER_ARTICLE_LINK", "文章链接样本");
define("_UPLOAD_ARTICLE_DESCRIPTION", "文章概述");
define("_UPLOAD_ARTICLE_BODY_TEXT", "文章内容");
define("_UPLOAD_ARTICLE_REFER_GOOGLE_KEYWORD", "什么是谷歌关键词?");
define("_UPLOAD_ARTICLE_REFER_DESCRIPTION", "文章概述会出现在哪里?");
define("_UPLOAD_ARTICLE_WHAT_IS_ARTICLE_LINK", "什么是文章链接/URL?");
define("_UPLOAD_ARTICLE_WHAT_IS_ARTICLE_LINK_EXP", "文章链接/URL是去往您的文章的链接。它不可以与其他文章链接重复或含有空格或任何特殊符号尤其是逗号句号开关引号。您可以使用-来分隔字体。若您使用的字体是用户经常在谷歌搜索引擎会查找的字眼，那么这将有助您的文章能更容易地被他人搜索到。");
define("_UPLOAD_ARTICLE_WHAT_IS_KEYWORD", "什么是谷歌关键词?");
define("_UPLOAD_ARTICLE_WHAT_IS_KEYWORD_EXP", "谷歌关键词就是当用户打在谷歌搜索要找的字眼，谷歌会把拥有该字眼的网站优先显示在上面。若您输入的关键词是用户经常会用来查找网站的字眼将有助您的文章更容易被找到。这个关键词只是辅助系统，不会出现在文章或网站的任何一个地方。您需要使用英文输入法的逗号,来分隔不同的关键词，中文输入法的逗号并不能起到作用。例子:健康,饮食,食谱,");
define("_UPLOAD_ARTICLE_WHAT_IS_DESCRIPTION", "文章概述会出现在哪里?");
define("_UPLOAD_ARTICLE_WHAT_IS_DESCRIPTION_EXP", "文章概述将会出现在主页和谷歌搜索引擎。它不会出现在文章正文里。");
define("_HEADER_UPLOAD_ARTICLE_NEW", "新");
define("_UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE", "如何嵌入Youtube视频？");
define("_UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE1", "点击iframe/地球图案。");
define("_UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE2", "点击Youtube视频下方的Share/分享。");
define("_UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE3", "点击embed/嵌入。");
define("_UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE4", "只复制该链接。");
define("_UPLOAD_ARTICLE_HOW_TO_EMBED_YOUTUBE5", "粘贴链接和点击OK!");
define("_UPLOAD_ARTICLE_HOW_TO_LINK", "如何输入链接？点击链接图案。");
define("_UPLOAD_ARTICLE_HOW_TO_LINK2", "输入Display Text，当用户点击这个Display Text就会去到该链接网站。将链接粘贴到URL格子。");
define("_UPLOAD_ARTICLE_HOW_TO_PLACE_IMAGE", "如何放入图片？点击图片图案。切记勿直接复制粘贴照片进正文。");
define("_UPLOAD_ARTICLE_HOW_TO_PLACE_IMAGE2", "右键图片并选择复制图片链接。");
define("_UPLOAD_ARTICLE_HOW_TO_PLACE_IMAGE3", "粘贴图片链接和点击OK!");
define("_UPLOAD_ARTICLE_BODY_TEXT_FAQ", "非常重要!切勿使用开关引号“”'',请根据指示上载照片！点击左手边的图案！");
define("_UPLOAD_ARTICLE_HOW_TO_EMBED_FB_VIDEO", "如何嵌入Facebook视频？");
define("_UPLOAD_ARTICLE_HOW_TO_EMBED_FB_VIDEO1", "请确保该视频设定是公开(Public)。点击分享(Share)后点击Embed.");
define("_UPLOAD_ARTICLE_HOW_TO_EMBED_FB_VIDEO2", "复制src开关引号里面的链接，请切记勿复制到开关引号和其他资料。 ");
//View Article
define("_VIEW_ARTICLES_ALL", "全部文章");
define("_VIEW_ARTICLES_DATE", "日期");
define("_VIEW_ARTICLES_AUTHOR", "作者");
define("_VIEW_ARTICLES_EDIT", "修改");
define("_VIEW_ARTICLES_DELETE", "删除");
define("_VIEW_ARTICLES_SHOW_HIDE", "删除");
define("_VIEW_ARTICLES_SHOW", "撤回删除");
define("_VIEW_ARTICLES_HIDE", "删除");
//Edit Article
define("_EDIT_ARTICLE", "修改文章");
define("_EDIT_ARTICLE_SUBMIT", "提交");
//Footer
define("_FOOTER_ALL_RIGHT", "店视吉版权所有");
define("_FOOTER_CREATE_ACCOUNT", "创建账号");