<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ArticleOne.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $articlesSocial = getArticlesOne($conn, " WHERE type = 'Social' ");
$articles = getArticlesOne($conn, " WHERE type = 'Travel' AND display = 'YES' ORDER BY date_created DESC ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:image" content="https://tevy.asia/img/fb-meta.jpg" />
<meta property="og:title" content="Travel | Tevy" />
<meta property="og:description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="keywords" content="Tevy, girls, female, lady, ladies, news, beauty care, beauty, skin care, fashion, social, etc">
<link rel="canonical" href="https://tevy.asia/travel.php" />
<title>Travel | Tevy</title>
<?php include 'css.php'; ?>

</head>
<body>
<?php include 'header-after-login.php'; ?>

<div class="background-div">

  <div class="cover-gap content min-height">

    <div class="test ">

      <h1 class="landing-h1"><?php echo _HEADER_TRAVEL ?></h1>	   
      <div class="big-white-div">

        <?php
        $conn = connDB();
        if($articles)
        {
          for($cnt = 0;$cnt < count($articles) ;$cnt++)
          {
          ?>
            <a href='article.php?id=<?php echo $articles[$cnt]->getArticleLink();?>'>
              <div class="article-card article-card-overwrite">
              	<a href='article.php?id=<?php echo $articles[$cnt]->getArticleLink();?>' class="overwrite-img-a"></a>
                <div class="article-bg-img-box">
                            	<a href="uploads/<?php echo $articles[$cnt]->getTitleCover();?>" class="progressive replace">
  									<img src="img/tiny.png" class="preview article-img1" alt="<?php echo $articles[$cnt]->getTitle();?>" title="<?php echo $articles[$cnt]->getTitle();?>"/>
								</a>                
                </div>

                <div class="box-caption box2">
                  <div class="wrap-a wrap100">
                    <a href='article.php?id=<?php echo $articles[$cnt]->getArticleLink();?>' class="peach-hover cate-a transition">
                      <?php echo _HEADER_TRAVEL ?> <span class="grey-text">• <?php echo $articles[$cnt]->getDateCreated();?></span>
                    </a>
                  </div>
                  <a href='article.php?id=<?php echo $articles[$cnt]->getArticleLink();?>'>
                      <div class="wrap-a wrap100 wrapm darkpink-hover article-title-a">
                        <?php echo $articles[$cnt]->getTitle();?>
                      </div>
    
                      <div class="text-content-div">
                        <?php 
                          $description = $articles[$cnt]->getKeywordOne();
                          $paragraphOne = $articles[$cnt]->getParagraphOne();

                          if($description != '')
                          {
                              echo $description;
                          }
                          else
                          {
                              echo $paragraphOne;
                          }
                        ?>

                      </div>
                  </a>
                </div>

              </div>
            </a>
          <?php
          }
          ?>
        <?php
        }
        $conn->close();
        ?>

      </div>

    </div>
  </div>

  <div class="clear"></div>

</div>
<?php include 'footer.php'; ?>

</body>
</html>